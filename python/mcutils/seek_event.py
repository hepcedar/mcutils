import os
from pyHepMC3 import HepMC3 as hm

class EventSeeker():
    '''
    The class that give the event according to the requirement. A findNext gives events sequencially and 
    findParticularEvent gives the exact event. Total amount of events can also be found out.
    '''
    def __init__(self,filename,filetype = 'hepmc3') -> None:
        '''
        Initialize the class and find the total amount of events the file has.

        Parameters :
        ------------
        filename : str
            The name of the hepmc3 file to find events on

        filetype : str
            The implementation method can be changed according to the filetype, currently only hepmc3 is 
            supported

        Returns :
        ---------
        None
        '''
        self.filename = filename
        self.filetype = filetype

        self.current_event = 0
        self.number_of_events = None

        self.findTotalEvents()

    def findTotalEvents(self):
        '''
        Find the total amount of particles of the file.

        Parameters :
        ------------
        None

        Returns :
        ---------
        None
        '''
        with open(self.filename, 'rb') as read_obj:
            read_obj.seek(0, os.SEEK_END)
            pointer_location = read_obj.tell()
            buffer = bytearray()

            event_number = 0
            foundLastEvent = False
            while pointer_location >= 0 and not foundLastEvent:
                read_obj.seek(pointer_location)
                pointer_location = pointer_location -1
                new_byte = read_obj.read(1)
                if new_byte == b'\n':
                    line = buffer.decode()[::-1]
                    if len(line) > 0:
                        if line[0] == 'E':
                            event_number = line.split(" ")[1]
                            foundLastEvent = True
                    buffer = bytearray()
                else:
                    buffer.extend(new_byte)
            
            if not foundLastEvent and len(buffer) > 0:
                line = buffer.decode()[::-1]
                if line[0] == 'E':
                    event_number = line.split(" ")[1]
                    foundLastEvent = True
        
        self.number_of_events = int(event_number) + 1

    def findParticularEvent(self,event_number):
        '''
        Return the event according to a event number, if the number is greater than total, the number 
        is wrapped around and made a part of the cyclic group.

        Parameters :
        ------------
        event_number : int
            The event number of the event required

        Returns :
        ---------
        evt : hm.GenVertex()
            The event corresponding with the event_number
        '''
        if event_number >= self.number_of_events:
            event_number%= self.number_of_events 
        
        evt = hm.GenEvent()
        f = hm.ReaderAscii(self.filename)
        f.skip(event_number - 1)
        f.read_event(evt)

        return evt

    def findNext(self):
        '''
        Return the next event according to what the current value of self.current_event is, 
        if the number is greater than total, the number is wrapped around and made a 
        part of the cyclic group.

        Parameters :
        ------------
        None

        Returns :
        ---------
        evt : hm.GenVertex()
            The next event according to self.current_event
        
        '''
        if self.current_event >= self.number_of_events:
            self.current_event%=self.number_of_events
        
        evt = self.findParticularEvent(self.current_event)
        self.current_event += 1
        
        return evt

    def getTotalEvents(self):
        '''
        A method to access the total amount of events

        Parameters :
        ------------
        None

        Returns :
        ---------
        self.number_of_events : int
            The total amount of events
        '''
        return self.number_of_events

def main():
    '''
    The driver code for testing the functionality
    '''
    e1 = EventSeeker('example.hepmc3','hepmc3')
    evt = e1.findNext()
    print(evt.event_number())
    evt = e1.findNext()
    print(evt.event_number())
    evt = e1.findNext()
    print(evt.event_number())
    evt = e1.findNext()
    print(evt.event_number())

    evt = e1.findParticularEvent(304)
    print(evt.event_number())

if __name__ == "__main__":
    main()