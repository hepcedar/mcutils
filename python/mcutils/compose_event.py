from pyHepMC3 import HepMC3 as hm

class EventComposer():
    '''
    The class that takes signal and pileup events and merges them to a
    single event. The GenRunInfo is taken from the signal event and the
    other events are added to it.
    '''

    def __init__(self, signalEvent, override=False):
        '''
        The initialization that takes a signal event and makes a GenEvent() which resembles
        the signal event, unless override is True, then only the GenRunInfo is copied.

        TODO: make a better structure than this override hack

        Parameters :
        ------------
        signalEvent : hm.GenEvent()
            The signal event whose details are used in GenRunInfo

        override : bool
            The flag to add the signalEvent vertex and particles in the new GenEvent()

        Returns :
        ---------
        None
        '''

        if not override:
            self.evt = signalEvent
            #self.evt = hm.GenEvent()
            #self.addEvent(signalEvent, issignal=True)
        else:
            self.evt = hm.GenEvent()

        # TODO: sort out the run object, to be shared between all events
        self.run = hm.GenRunInfo()
        #self.evt.set_run_info(self.run)
        self.num_pileup = 0

        # Specify which attribute type is used
        runInfo = {"signal_process_id"     : hm.IntAttribute,
                   "signal_process_vertex" : hm.IntAttribute,
                   "event_scale"           : hm.DoubleAttribute,
                   "alphaQCD"              : hm.DoubleAttribute,
                   "alphaQED"              : hm.DoubleAttribute,
                   "mpi"                   : hm.IntAttribute}

        for k in runInfo.keys():
            # Get attribute and test for non-emptiness
            attr = signalEvent.attribute(k)
            if not attr:
                continue

            # First convert to float and if it is integer, then convert it to integer
            try:
                attr = float(attr)
                if attr.is_integer():
                    attr = int(attr)
            except:
                pass

            self.evt.add_attribute(k, runInfo[k](attr))


        # Creating a GenCrossSection and get the results from the signalEvent.
        # The result is in the form of a list, which is space-separated, so need to
        # split it to list and then convert to float.

        # If the value is integer, then again convert the float to int. Same with GenPdfInfo
        newCrossSection = hm.GenCrossSection()
        csValues = [int(x) if x.is_integer() else x
                  for x in [float(x) for x in
                  signalEvent.attribute("GenCrossSection").split(' ')]]

        # Cross-section from signal
        #newCrossSection.set_cross_section(*csValues)
        newCrossSection.set_cross_section(csValues[0], 0.0)
        self.evt.add_attribute("GenCrossSection", newCrossSection)

        # PDFs from signal
        newPdfInfo = hm.GenPdfInfo()
        csValues = [int(x) if x.is_integer() else x
                  for x in [float(x) for x in
                  signalEvent.attribute("GenPdfInfo").split(' ')]]

        newPdfInfo.set(*csValues)
        self.evt.add_attribute("GenPdfInfo", newPdfInfo)

        self.evt.set_event_number(signalEvent.event_number())


    def addEvent(self, puevt, issignal=False):
        '''
        Adding the other pileup files by adding vertices and
        particles to the signalEvent.

        Parameters :
        ------------
        puevt : hm.GenEvent()
            A pile-up event to add to the already made self.evt

        Returns :
        ---------
        None
        '''

        # TODO: add the beam particles

        # TODO: check weights

        # Increment the PU counter
        if not issignal:
            self.num_pileup += 1

            # Add a special vertex for attaching this PU event to
            # TODO: add vertex offsets
            vtx_pu = hm.GenVertex()
            prt_pu = hm.GenParticle()
            prt_pu.set_status(11)
            prt_pu.set_pid(80+self.num_pileup)
            vtx_pu.add_particle_in(prt_pu)
            self.evt.add_vertex(vtx_pu)
            #print(f"#P,V = {self.evt.particles_size():d},{self.evt.vertices_size():d}")

            num_pu_particles = 0
            for particle in puevt.particles():
                if particle.status() != 1:
                    continue
                num_pu_particles += 1
                temp_particle = hm.GenParticle(particle.momentum(), particle.pid(), particle.status())
                #print(f"Adding particle #{num_pu_particles:d} in PU event #{self.num_pileup:d}")
                # TODO: randomly rotate the event around phi
                # TODO: sample event frame-boosts
                vtx_pu.add_particle_out(temp_particle)


        else: # if signal
            # TODO: surely there's a more efficient way of copying the signal event... or just using the read-in one?
            #return

            # self.run.set_weight_names(["Nominal"])
            #self.evt.weights()[0] = puevt.weight()
            self.evt.set_cross_section(puevt.cross_section())

            vertex_map = {}
            for vertex in puevt.vertices():

                temp_vertex = hm.GenVertex(vertex.position())
                #temp_vertex.set_id(vertex.id())
                temp_vertex.set_status(10) #vertex.status())

                vertex_map[vertex] = temp_vertex
                self.evt.add_vertex(temp_vertex)

            for particle in puevt.particles():
                temp_particle = hm.GenParticle(particle.momentum(), particle.pid(), particle.status())

                if particle.end_vertex() is not None:
                    vertex_map[particle.end_vertex()].add_particle_in(temp_particle)

                if particle.production_vertex() is not None:
                    # TODO: Find out why GenEvent does not contain all the vertices
                    if particle.production_vertex() in vertex_map:
                        vertex_map[particle.production_vertex()].add_particle_out(temp_particle)


    def getEvent(self):
        '''
        Returns the generated GenEvent

        TODO: remove, this is pointless

        Parameters :
        ------------
        None

        Returns :
        ---------
        self.evt : hm.GenEvent()
            The generated event
        '''
        return self.evt


    def setEventNumber(self,value):
        '''
        Allow change the value of the event number.

        TODO: remove, this is pointless

        Parameters :
        ------------
        value : int
            The value of the new event number.

        Returns :
        ---------
        None
        '''
        self.evt.set_event_number(int(value))



# def main():
#     '''
#     The driver code for testing the class
#     '''
#     f1 = hm.ReaderAscii('/home/blizzard/pilemc/pypilup/data/example.hepmc3')
#     f2 = hm.WriterAscii('trial.hepmc3')

#     evt = hm.GenEvent()
#     f1.read_event(evt)
#     e1 = EventComposer(evt)

#     for _ in range(10):
#         evt = hm.GenEvent()
#         f1.read_event(evt)
#         e1.addEvent(evt,False)
#         print(evt.event_number())
#         f2.write_event(e1.evt)

# if __name__ == "__main__":
#     main()
