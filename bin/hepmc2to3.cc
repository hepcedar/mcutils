// -*- C++ -*-
//
// This file is part of MCUtils -- https://gitlab.com/hepcedar/mcutils/
// Copyright (C) 2013-2024 Andy Buckley <andy.buckley@cern.ch>
//
// Embedding of MCUtils code in other projects is permitted provided this
// notice is retained and the MCUtils namespace and include path are changed.
//

#include "MCUtils/HepMCUtils.h"
#include <memory>
using namespace std;

/// @file Converter from HepMC2 to HepMC3 ASCII format
/// @author Andy Buckley <andy.buckley@cern.ch>

#include "HepMC3/ReaderFactory.h"
#include "HepMC3/WriterAscii.h"
#include "HepMC3/WriterAsciiHepMC2.h"

int main(int argc, char** argv) {

  // Configure input and output event files from the command-line args
  string fmt = argv[1];
  string infile = argv[2];
  string outfile = infile.substr(0, infile.rfind(".")) + ("-"+fmt+".hepmc");
  if (argc > 3) outfile = argv[2];

  // Create I/O objects
  std::shared_ptr<HepMC3::Reader> in = HepMC3::deduce_reader(infile);
  std::shared_ptr<HepMC3::Writer> out = (fmt == "3") ?
    std::static_pointer_cast<HepMC3::Writer>(std::make_shared<HepMC3::WriterAscii>(outfile)) :
    std::static_pointer_cast<HepMC3::Writer>(std::make_shared<HepMC3::WriterAsciiHepMC2>(outfile));

  // Event loop
  int nevt = 0;
  HepMC3::GenEvent ge;
  while (1) {
    nevt += 1;

    // Read event
    in->read_event(ge);
    if (in->failed()) break;

    // // Event properties before filtering
    // const int particles_size_orig = ge->particles_size();
    // const int vertices_size_orig = ge->vertices_size();
    // const int fs_size_orig = std::count_if(ge->particles().begin(), ge->particles().end(), MCUtils::isStable);

    // Write event and clear the holder
    out->write_event(ge);
    ge.clear();
  }

  return 0;
}
