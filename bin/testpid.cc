#include "MCUtils/PIDUtils.h"
using namespace MCUtils;
#include <iostream>
#include <string>
using namespace std;


int main(int argc, char** argv) {
  if (argc < 2) return 0;

  vector<int> pids;
  const bool testing = (argv[1] == string("test"));
  if (testing) {
    for (int i = -100000000; i <= 100000000; i++) pids.push_back(i);
  } else {
    for (int j = 1; j < argc; j++) pids.push_back(atoi(argv[j]));
  }
  // cout << "Testing " << pids.size() << " PID codes" << endl << endl;

  #define MSG(x) if (!testing) cout << x << endl
  for (int pid : pids) {
    MSG("PID = " << pid);
    const bool valid = PID::isValid(pid);
    MSG((valid ? "VALID" : "INVALID"));
    if (!valid && !testing) exit(1);

    if (PID::isNucleus(pid)) MSG("Nucleus");
    if (PID::isBSM(pid)) MSG("BSM");
    if (PID::isLepton(pid)) MSG("Lepton");
    if (PID::isPhoton(pid)) MSG("Photon");
    if (PID::isHadron(pid)) MSG("Hadron");
    if (PID::isParton(pid)) MSG("Parton");
    if (PID::isQuark(pid)) MSG("Quark");
    if (PID::isGluon(pid)) MSG("Gluon");
    if (PID::isDiquark(pid)) MSG("Diquark");
    if (PID::isMeson(pid)) MSG("Meson");
    if (PID::isBaryon(pid)) MSG("Baryon");
    if (PID::hasCharm(pid)) MSG("Has charm");
    if (PID::hasBottom(pid)) MSG("Has bottom");
    MSG("2J+1 = " << PID::jSpin(pid) << ", " <<
        "2S+1 = " << PID::sSpin(pid) << ", " <<
        "2L+1 = " << PID::lSpin(pid));
    MSG("Q = " << PID::threeCharge(pid) << "/3 = " << PID::charge(pid));
    MSG("Coloured = " << boolalpha << PID::isStrongInteracting(pid));
    // MSG(endl);
  }

  return 0;
}
