// -*- C++ -*-
//
// This file is part of MCUtils -- https://gitlab.com/hepcedar/mcutils/
// Copyright (C) 2013-2022 Andy Buckley <andy.buckley@cern.ch>
//
// Embedding of MCUtils code in other projects is permitted provided this
// notice is retained and the MCUtils namespace and include path are changed.
//
/// @file Demo of filtering a HepMC record
/// @author Andy Buckley <andy.buckley@cern.ch>
//
// NOTE: Requires HepMC. Compile with e.g.
//  g++ -o hepmcreduce hepmcreduce.cc -Iinclude -I/path/to/hep/installs/include -L/path/to/hep/installs/lib -lHepMC3

#include "MCUtils/HepMCUtils.h"
//#include "MCUtils/Clustering.h" //< Unnecessary, but forces compile-time testing of that header!
using namespace std;


#ifdef MCUTILS_HEPMC3
using XGenEvent = HepMC3::GenEvent;
using XGenParticlePtr = HepMC3::GenParticlePtr;
using XConstGenParticlePtr = HepMC3::ConstGenParticlePtr;
#include "HepMC3/ReaderFactory.h"
#include "HepMC3/WriterAscii.h"
#include "HepMC3/WriterAsciiHepMC2.h"
#else
using XGenEvent = HepMC::GenEvent;
using XGenParticlePtr = HepMC::GenParticlePtr*;
using XConstGenParticlePtr = const HepMC::GenParticlePtr*;
#include "HepMC/IO_GenEvent.h"
#endif


// A classifier function for GenParticles we want to remove
bool find_bad_particles(XConstGenParticlePtr p) {
  // Remove "null" particles used for HEPEVT padding
  if (p->pdg_id() == 0) return true;
  // Always keep EW particles, BSM particles, and heavy flavour partons & hadrons
  /// @todo Also keep particles directly connected to EW & BSM objects?
  /// @todo Intermediate replica removal?
  if (MCUtils::isResonance(p)) return false;
  if (MCUtils::isBSM(p)) return false;
  if (MCUtils::isHeavyFlavour(p)) return false;
  // Kill (simple) loops
  //if (p->production_vertex() == p->end_vertex() && p->end_vertex() != NULL) return true;
  // Remove G4-unsafe particles in decay chains
  if ((MCUtils::fromDecay(p) && !MCUtils::isTransportable(p))) return true;
  // Remove unstable hadrons from decay chains not containing heavy flavour or taus
  if (MCUtils::isDecayed(p) && !(MCUtils::isParton(p) || MCUtils::fromTauOrHFDecay(p))) return true;
  // Remove partons (other than those already preserved)
  if (MCUtils::isParton(p)) return true;
  // Remove non-standard particles
  if (MCUtils::isGenSpecific(p)) return true;
  if (MCUtils::isDiquark(p)) return true;
  //if (MCUtils::hasNonStandardStatus(p)) return true;
  // Apply cuts on |eta| and pT (to be used only for thinning G4 particles)
  //if (p->barcode() > 200000 && (!MCUtils::InEtaRange(-5.0, 5.0)(p) || !MCUtils::PtGtr(0.3)(p))) return true;
  // Cut out all beampipe and super-soft particles
  if (!MCUtils::InEtaRange(-6.0, 6.0)(p)) return true;
  if (!MCUtils::PtGtr(1e-3)(p)) return true;
  // Remove stable particles (to remove clutter for debug visualisation only!!!)
  //if (MCUtils::isStable(p)) return true;
  //if (MCUtils::isStable(p) && (p->pdg_id() == 111 || abs(p->pdg_id()) == 211)) return true;
  //if (MCUtils::isStable(p) && p->pdg_id() == 22) return true;
  return false;
}

// A demo classifier function for GenParticles we want to remove from MC pile-up overlay
bool find_bad_particles_pileup(XConstGenParticlePtr p) {
  // Remove "null" particles used for HEPEVT padding
  if (p->pdg_id() == 0) return true;
  // CHECK
  // if (p->pdg_id() == 111 || p->pdg_id() == 311) {
  //   std::cout << "** " << MCUtils::isBeam(p) << MCUtils::isHeavyFlavour(p) << MCUtils::isDecayed(p) << std::endl;
  // }
  // Don't remove the beams
  if (MCUtils::isBeam(p)) return false;
  // Remove unstable hadrons except HF (but not keeping HF intermediate decay products)
  if (MCUtils::isHeavyFlavour(p)) return false;
  if (MCUtils::isDecayed(p)) return true;
  // Remove partons (other than those already preserved)
  if (MCUtils::isParton(p)) return true;
  // Remove non-standard particles
  if (MCUtils::isGenSpecific(p)) return true;
  if (MCUtils::isDiquark(p)) return true;
  // Cut out all beampipe and super-soft particles
  if (!MCUtils::InEtaRange(-5.0, 5.0)(p)) return true;
  if (!MCUtils::PtGtr(1e-2)(p)) return true;
  return false;
}


int main(int argc, char** argv) {
  // Configure input and output event files from the command-line args
  string infile = "in.hepmc";
  if (argc > 1) infile = argv[1];
  string outfile = infile.substr(0, infile.rfind(".")) + "-reduced.hepmc";
  if (argc > 2) outfile = argv[2];

  // Create I/O objects
  #ifdef MCUTILS_HEPMC3
  std::shared_ptr<HepMC3::Reader> in = HepMC3::deduce_reader(infile);
  HepMC3::WriterAscii out(outfile); /// @todo Also allow writing to WriterAsciiHepMC2 or ROOT or...
  #else
  HepMC::IO_GenEvent in(infile, ios::in);
  HepMC::IO_GenEvent out(outfile, ios::out);
  #endif

  // Event loop
  int nevt = 0;
  #ifdef MCUTILS_HEPMC3
  XGenEvent actualge;
  XGenEvent* ge = &actualge;
  in->read_event(*ge);
  #else
  XGenEvent* ge = in.read_next_event();
  #endif

  while (1) {
    nevt += 1;

    // Event properties before filtering
    const int particles_size_orig = ge->particles_size();
    const int vertices_size_orig = ge->vertices_size();
    const int fs_size_orig = std::count_if(ge->particles().begin(), ge->particles().end(), MCUtils::isStable);
    #ifdef CHECKING
    const int num_orphan_vtxs_orig = MCUtils::const_vertices_match(ge, MCUtils::isDisconnected).size();
    const int num_noparent_vtxs_orig = MCUtils::const_vertices_match(ge, MCUtils::hasNoParents).size();
    const int num_nochild_vtxs_orig = MCUtils::const_vertices_match(ge, MCUtils::hasNoChildren).size();
    #endif

    // Consistently remove the unwanted particles from the event
    MCUtils::ireduce(ge, find_bad_particles_pileup);

    // Event properties after filtering
    const int particles_size_filt = ge->particles_size();
    const int vertices_size_filt = ge->vertices_size();
    const int fs_size_filt = std::count_if(ge->particles().begin(), ge->particles().end(), MCUtils::isStable);
    #ifdef CHECKING
    const int num_orphan_vtxs_filt = MCUtils::const_vertices_match(ge, MCUtils::isDisconnected).size();
    const int num_noparent_vtxs_filt = MCUtils::const_vertices_match(ge, MCUtils::hasNoParents).size();
    const int num_nochild_vtxs_filt = MCUtils::const_vertices_match(ge, MCUtils::hasNoChildren).size();
    #endif

    // Write out the change in the number of particles, etc.
    cout << "#" << nevt << ": "
         << particles_size_orig << " (" << fs_size_orig <<  "), " << vertices_size_orig << " -> "
         << particles_size_filt << " (" << fs_size_filt <<  "), " << vertices_size_filt << endl;
    #ifdef CHECKING
    if (num_orphan_vtxs_filt != num_orphan_vtxs_orig)
      cerr << "WARNING! Change in num orphaned vertices: "
           << num_orphan_vtxs_orig << " -> " << num_orphan_vtxs_filt << endl;
    if (num_noparent_vtxs_filt != num_noparent_vtxs_orig)
      cerr << "WARNING! Change in num no-parent vertices: "
           << num_noparent_vtxs_orig << " -> " << num_noparent_vtxs_filt << endl;
    if (num_nochild_vtxs_filt != num_nochild_vtxs_orig)
      cerr << "WARNING! Change in num no-child vertices: "
           << num_nochild_vtxs_orig << " -> " << num_nochild_vtxs_filt << endl;
    #endif

    // Write out reduced event, read next, and exit if done
    #if MCUTILS_HEPMC3
    out.write_event(*ge);
    ge->clear();
    in->read_event(*ge);
    if (in->failed()) break;
    #else
    out << ge;
    delete ge;
    in >> ge;
    if (!ge) break;
    #endif

    //break;
  }

  return 0;
}
