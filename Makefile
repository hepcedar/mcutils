## Makefile for MCUtils
## Execute as 'make HEPUTILS_PREFIX=/path/to/heputils HEPMC_PREFIX=/path/to/hepmc', 'make install', 'make clean'

VERSION := 2.0.0beta1
CXX := g++
CXXFLAGS := -O2 -std=c++17

## Include the config
include Makefile.inc


.PHONY = exec check install uninstall dist clean distclean doxy

exec:
	cd bin/ && $(MAKE)

check: bin/testpid
	./check-testpids.sh

test-hepmc:
	$(CXX) tests/HepFV2P4.cc -o HepFV2P4 -I../include $(HEPMC_CPPFLAGS) $(HEPUTILS_CPPFLAGS) $(HEPMC_LIBFLAGS) -Wall -pedantic && ./HepFV2P4
	$(CXX) tests/childrenParticleUtils.cc -o childrenParticleUtils -I../include $(HEPMC_CPPFLAGS) $(HEPUTILS_CPPFLAGS) $(HEPMC_LIBFLAGS) -Wall -pedantic && ./childrenParticleUtils
	$(CXX) tests/parentsParticleUtils.cc -o parentsParticleUtils -I../include $(HEPMC_CPPFLAGS) $(HEPUTILS_CPPFLAGS) $(HEPMC_LIBFLAGS) -Wall -pedantic && ./parentsParticleUtils

install: exec
	mkdir -p $(PREFIX)/include && cp -r include/* $(PREFIX)/include/
	(cd python && make install)
	(cd bin && make install)

uninstall:
	rm -rf $(PREFIX)/include/MCUtils

doxy:
	doxygen

dist: doxy
	tar czf MCUtils-$(VERSION).tar.gz README TODO ChangeLog Makefile Doxyfile include/ bin/ python/ doxygen/

clean:
	cd bin/ && $(MAKE) clean
	rm -rf doxygen

distclean: clean
	rm -f MCUtils-$(VERSION).tar.gz
