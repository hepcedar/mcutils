// -*- C++ -*-
//
// This file is part of MCUtils -- https://gitlab.com/hepcedar/mcutils/
// Copyright (C) 2013-2024 Andy Buckley <andy.buckley@cern.ch>
//
// Embedding of MCUtils code in other projects is permitted provided this
// notice is retained and the MCUtils namespace and include path are changed.
//
#ifndef MCUTILS_HEPMCEVENTUTILS_H
#define MCUTILS_HEPMCEVENTUTILS_H

/// @file Functions for navigating HepMC record contents
/// @author Andy Buckley <andy.buckley@cern.ch>


#include <vector>
#include <stdexcept>
#include <cassert>
#include <algorithm>
#include <type_traits>
#ifdef MCUTILS_HEPMC3
#include "HepMC3/GenEvent.h"
#include "HepMC3/GenVertex.h"
#include "HepMC3/GenParticle.h"
#include "HepMC3/Relatives.h"
#else
#include "HepMC/GenEvent.h"
#endif

#include "MCUtils/HepMCParticleUtils.h"

#include "HEPUtils/Utils.h"
#include "MCUtils/HepMCParticleUtils.h"
#include "MCUtils/HepMCVertexUtils.h"


namespace MCUtils {

#ifdef MCUTILS_HEPMC3


/// @name Friendlier HepMC type typedefs
/// @{
typedef std::vector<HepMC3::GenVertexPtr> GenVertices;
typedef std::vector<HepMC3::ConstGenVertexPtr> ConstGenVertices;

typedef std::shared_ptr<HepMC3::GenEvent> GenEventPtr;
typedef std::shared_ptr<const HepMC3::GenEvent> ConstGenEventPtr;

typedef std::vector<HepMC3::GenParticlePtr> GenParticles;
typedef std::shared_ptr<const HepMC3::GenParticle> ConstGenParticlePtr;
typedef std::vector<HepMC3::ConstGenParticlePtr> ConstGenParticles;

/// @}

/// Ensure that T is pointer of following types
template<class T>
using is_GEP = typename std::enable_if<std::is_same<GenEventPtr,T>::value ||
                                       std::is_same<ConstGenEventPtr,T>::value ||
                                       std::is_same<HepMC3::GenEvent*,T>::value ||
                                       std::is_same<const HepMC3::GenEvent*,T>::value, bool>::type;

/// @name Event constness converters
///
/// @note These are just syntactic sugar: this is already not so inconvenient!
/// @{
template<class T, is_GEP<T> = true>
inline auto mk_const(T ge) {
    if constexpr (std::is_same<T, HepMC3::GenEvent*>()) {
        return const_cast<const HepMC3::GenEvent*>(ge);
    } else {
        return std::const_pointer_cast<const HepMC3::GenEvent>(ge);
    }
}

template<class T>
inline auto mk_unconst(T ge, is_GEP<T> = true) {
    if constexpr (std::is_same<T, const HepMC3::GenEvent*>()) {
        return const_cast<HepMC3::GenEvent*>(ge);
    } else {
        return std::const_pointer_cast<HepMC3::GenEvent>(ge);
    }
}
/// @}


/// @brief Get the event weight
///
/// Defaults to the first weight if there are several, 1 if there are none.
///
/// @note Events may increasingly have multiple physically meaningful weights,
/// expressing scale, PDF, etc. systematic variations.
///
/// @todo Generalise/overload for string indices
template <class T, is_GEP<T> = true>
inline double get_weight(T evt, size_t index=0) {
    if (evt->weights().empty()) return 1.0;
    if (index >= evt->weights().size()) throw std::runtime_error("Requested weight array index out of bounds");
    return evt->weights()[index];
}


/// @name Convenience functions for particle/vertex looping
/// @todo Provide functions for particles in/out, different relations, only gen-stable particles, accepting predicates, etc.
/// @{

/// @todo Remove reference-based functions -- always use pointers for HepMC objects?

/// Get a vector of GenParticle pointers on which to iterate
inline GenParticles particles(const HepMC3::GenEvent& ge) {
    std::vector<HepMC3::GenParticlePtr> vp;
    vp.reserve(ge.particles().size());
    for (auto cp : ge.particles()) {
        vp.push_back(std::const_pointer_cast<HepMC3::GenParticle>(cp));
    }
    return vp;
}

/// Get a vector of GenParticle pointers on which to iterate
template <class T, is_GEP<T> = true>
inline GenParticles particles(T ge) {
    std::vector<HepMC3::GenParticlePtr> vp;
    vp.reserve(ge->particles().size());
    for (auto cp : ge->particles()) {
        vp.push_back(std::const_pointer_cast<HepMC3::GenParticle>(cp));
    }
    return vp;
}


/// Get a vector of const GenParticle pointers
inline ConstGenParticles const_particles(const HepMC3::GenEvent& ge) {
    return ge.particles();
}


/// Get a vector of const GenParticle pointers on which to iterate
template <class T, is_GEP<T> = true>
inline ConstGenParticles const_particles(T ge) {
    return const_particles(*ge);
}

template <class T>
inline GenVertices vertices(T ge) {
    std::vector<HepMC3::GenVertexPtr> vp;
    vp.reserve(ge->vertices().size());
    for (auto cp : ge->vertices()) {
        vp.push_back(std::const_pointer_cast<HepMC3::GenVertex>(cp));
    }
    return vp;
}

inline GenVertices vertices(const HepMC3::GenEvent& ge) {
    std::vector<HepMC3::GenVertexPtr> vp;
    vp.reserve(ge.vertices().size());
    for (auto cp : ge.vertices()) {
        vp.push_back(std::const_pointer_cast<HepMC3::GenVertex>(cp));
    }
    return vp;
}

/// Get a vector of GenVertex pointers on which to iterate
inline ConstGenVertices const_vertices(const HepMC3::GenEvent& ge) {
    return ge.vertices();
}


/// Get a vector of const GenVertex pointers on which to iterate
template <class T>
inline ConstGenVertices const_vertices(T ge) {
    static_assert(std::is_same<T, GenEventPtr>::value || std::is_same<T, HepMC3::GenEvent*>::value,
                  "Function can only be used for GenEvent pointer");
    return const_vertices(*ge);
}


/// Get a vector of GenParticle pointers
///
/// @a String str is one of descendants, ancestors, children, grandchildren, parents, grandparents
template<typename T> std::vector<T> particles(T gv, string str) {
    std::transform(str.begin(), str.end(), str.begin(), ::tolower);
    if (str == "descendants") {
        return HepMC3::descendant_particles(gv);
    } else if (str == "ancestors") {
        return HepMC3::ancestor_particles(gv);
    } else if (str == "grandchildren") {
        return HepMC3::grandchildren_particles(gv);
    } else if (str == "grandparents") {
        return HepMC3::grandparent_particles(gv);
    } else throw std::invalid_argument("Choose from: children, parents, grandchildren, grandparents, descendants, ancestors");
}

/// Get a vector of GenParticle pointers on which to iterate
///
/// @a String str is one of descendants, ancestors, children, grandchildren, parents, grandparents
inline GenParticles particles(HepMC3::GenVertexPtr gv, string str) {
    std::transform(str.begin(), str.end(), str.begin(), ::tolower);
    if (str == "children") {
        return HepMC3::children_particles(gv);
    } else if (str == "parents") {
        return HepMC3::parent_particles(gv);
    } else throw std::invalid_argument("String input. Choose from: children, parents, grandchildren, grandparents, descendants, ancestors");
}

inline ConstGenParticles particles(HepMC3::ConstGenVertexPtr gv, string str) {
    std::transform(str.begin(), str.end(), str.begin(), ::tolower);
    if (str == "children") {
        return HepMC3::children_particles(gv);
    } else if (str == "parents") {
        return HepMC3::parent_particles(gv);
    } else throw std::invalid_argument("String input. Choose from: children, parents, grandchildren, grandparents, descendants, ancestors");
}

/// Get a vector of const GenParticle pointers on which to iterate
///
inline ConstGenParticles const_particles(const HepMC3::GenVertex& gv, string str) {
    HepMC3::ConstGenVertexPtr gvCPtr = std::make_shared<const HepMC3::GenVertex>(gv);
    return particles(gvCPtr, str);
}
/// Get a vector of const GenParticle pointers on which to iterate
///
inline ConstGenParticles const_particles(HepMC3::ConstGenVertexPtr gv, string str) {
    assert(gv);
    return const_particles(*gv, str);
}


/// @name Convenience-named particle relative access functions
/// @{

inline GenParticles children(HepMC3::GenVertex& gv) {
    HepMC3::GenVertexPtr gvPtr = std::make_shared<HepMC3::GenVertex>( gv );
    return particles(gvPtr, "children");
}
inline GenParticles children(HepMC3::GenVertexPtr gv) {
    return particles(gv, "children");
}
inline ConstGenParticles const_children(const HepMC3::GenVertex& gv) {
    HepMC3::GenVertexPtr gvPtr = std::make_shared<HepMC3::GenVertex>( gv );
    return const_particles(gvPtr, "children");
}
inline ConstGenParticles const_children(HepMC3::ConstGenVertexPtr gv) {
    return const_particles(gv, "children");
}
//
inline GenParticles parents(HepMC3::GenVertex& gv) {
    HepMC3::GenVertexPtr gvPtr = std::make_shared<HepMC3::GenVertex>( gv );
    return particles(gvPtr, "parents");
}
inline GenParticles parents(HepMC3::GenVertexPtr gv) {
    return particles(gv, "parents");
}
inline ConstGenParticles const_parents(const HepMC3::GenVertex& gv) {
    HepMC3::GenVertexPtr gvPtr = std::make_shared<HepMC3::GenVertex>( gv );
    return const_particles(gvPtr, "parents");
}
inline ConstGenParticles const_parents(HepMC3::ConstGenVertexPtr gv) {
    return const_particles(gv, "parents");
}
//
inline GenParticles descendants(HepMC3::GenVertex& gv) {
    HepMC3::GenVertexPtr gvPtr = std::make_shared<HepMC3::GenVertex>( gv );
    return particles(gvPtr, "descendants");
}
inline GenParticles descendants(HepMC3::GenVertexPtr gv) {
    return particles(gv, "descendants");
}
inline ConstGenParticles const_descendants(const HepMC3::GenVertex& gv) {
    HepMC3::GenVertexPtr gvPtr = std::make_shared<HepMC3::GenVertex>( gv );
    return const_particles(gvPtr, "descendants");
}
inline ConstGenParticles const_descendants(HepMC3::ConstGenVertexPtr gv) {
    return const_particles(gv, "descendants");
}
//
inline GenParticles ancestors(HepMC3::GenVertex& gv) {
    HepMC3::GenVertexPtr gvPtr = std::make_shared<HepMC3::GenVertex>( gv );
    return particles(gvPtr, "ancestors");
}
inline GenParticles ancestors(HepMC3::GenVertexPtr gv) {
    return particles(gv, "ancestors");
}
inline ConstGenParticles const_ancestors(const HepMC3::GenVertex& gv) {
    HepMC3::GenVertexPtr gvPtr = std::make_shared<HepMC3::GenVertex>( gv );
    return const_particles(gvPtr, "ancestors");
}
inline ConstGenParticles const_ancestors(HepMC3::ConstGenVertexPtr gv) {
    return const_particles(gv, "ancestors");
}

/// Similar functions for particle relative access direct from GenParticle rather than GenVertex
inline GenParticles children(HepMC3::GenParticle& gp) {
    return (gp.end_vertex() != NULL) ? particles(gp.end_vertex(), "children") : GenParticles();
}
inline GenParticles children(HepMC3::GenParticlePtr gp) {
    return MCUtils::children(*gp);
}
inline ConstGenParticles const_children(const HepMC3::GenParticle& gp) {
    return (gp.end_vertex() != NULL) ? const_particles(gp.end_vertex(), "children") : ConstGenParticles();
}
inline ConstGenParticles const_children(HepMC3::ConstGenParticlePtr gp) {
    return const_children(*gp);
}

inline GenParticles parents(HepMC3::GenParticle& gp) {
    return (gp.production_vertex() != NULL) ? particles(gp.production_vertex(), "parents") : GenParticles();
}
inline GenParticles parents(HepMC3::GenParticlePtr gp) {
    return MCUtils::parents(*gp);
}
inline ConstGenParticles const_parents(const HepMC3::GenParticle& gp) {
    return (gp.production_vertex() != NULL) ? const_particles(gp.production_vertex(), "parents") : ConstGenParticles();
}
inline ConstGenParticles const_parents(HepMC3::ConstGenParticlePtr gp) {
    return const_parents(*gp);
}
//
inline GenParticles descendants(HepMC3::GenParticle& gp) {
    return (gp.end_vertex() != NULL) ? particles(gp.end_vertex(), "descendants") : GenParticles();
}
inline GenParticles descendants(HepMC3::GenParticlePtr gp) {
    return MCUtils::descendants(*gp);
}
inline ConstGenParticles const_descendants(const HepMC3::GenParticle& gp) {
    return (gp.end_vertex() != NULL) ? const_particles(gp.end_vertex(), "descendants") : ConstGenParticles();
}
inline ConstGenParticles const_descendants(HepMC3::ConstGenParticlePtr gp) {
    return const_descendants(*gp);
}
//
inline GenParticles ancestors(HepMC3::GenParticle& gp) {
    return (gp.production_vertex() != NULL) ? particles(gp.production_vertex(), "ancestors") : GenParticles();
}
inline GenParticles ancestors(HepMC3::GenParticlePtr gp) {
    return MCUtils::ancestors(*gp);
}
inline ConstGenParticles const_ancestors(const HepMC3::GenParticle& gp) {
    return (gp.production_vertex() != NULL) ? const_particles(gp.production_vertex(), "ancestors") : ConstGenParticles();
}
inline ConstGenParticles const_ancestors(HepMC3::ConstGenParticlePtr gp) {
    return const_ancestors(*gp);
}

/// @}

//helping functions for following function
inline HepMC3::ConstGenVertexPtr signal_process_vertex(ConstGenEventPtr genEvent) {
    for (auto v: genEvent->vertices()) if (v->attribute<HepMC3::IntAttribute>("signal_process_vertex")) return v;
    return nullptr;
}
inline HepMC3::GenVertexPtr signal_process_vertex(GenEventPtr genEvent) {
    for (auto v: genEvent->vertices()) if (v->attribute<HepMC3::IntAttribute>("signal_process_vertex")) return v;
    return nullptr;
}


/// @name High-level event handling functions
/// @{

/// Find the (most likely) primary vertex position for this event
template <class T, is_GEP<T> = true>
inline HepMC3::FourVector primaryVertex(T genEvent) {
    // Get the decay vertex of the beam particle
    if (!genEvent->beams().empty()) {
        HepMC3::ConstGenParticlePtr p = genEvent->beams().front();
        HepMC3::ConstGenVertexPtr v = p->end_vertex();
        if (v != NULL) return v->position();
        else std::cout << "The beam particles have no end vertex" << std::endl;
    }
    // Or get the signal process vertex position
    else if (signal_process_vertex(genEvent) != nullptr) {
        return signal_process_vertex(genEvent)->position();
    }
    // Or get the production vertex of a final particle whose status is not 1 or 2
    else if (!genEvent->particles_empty()) {
        for (auto p : genEvent->particles()) {
            if (p->status() != 1 && p->status() != 2) {
                if (p->production_vertex() != NULL) return p->production_vertex()->position();
            }
        }
    }
    // Or get the vertex (either begin or end!) of the particle with barcode == 1
    else {
        HepMC3::ConstGenParticlePtr p;
        for (auto partPtr : genEvent->particles()) {
            if (partPtr->id() == 1) {
                HepMC3::ConstGenParticlePtr p = partPtr;
            }
        }
        if (p != NULL) {
            if (p->production_vertex() != NULL) return p->production_vertex()->position();
            else if (p->end_vertex() != NULL) return p->end_vertex()->position();
            else std::cerr << "The first particle has no production vertex and no end vertex" << std::endl;
        }
        else std::cerr << "No particle with barcode 1" << std::endl;
    }
    // No particle with a primary vertex was found, hence returning pv(0,0,0)
    std::cerr << "Primary vertex could not be determined" << std::endl;
    return HepMC3::FourVector(0., 0., 0., 0.);
}

/// @}


#else
/// @name Friendlier HepMC type typedefs
/// @{
typedef std::vector<HepMC::GenVertex*> GenVertices;
typedef std::vector<const HepMC::GenVertex*> GenVerticesC;
/// @}

/// @name Event constness converters
///
/// @note These are just syntactic sugar: this is already not so inconvenient!
/// @{
inline const HepMC::GenEvent* mk_const(HepMC::GenEvent* ge) {
    return const_cast<const HepMC::GenEvent*>(ge);
}
inline HepMC::GenEvent* mk_unconst(const HepMC::GenEvent* const_ge) {
    return const_cast<HepMC::GenEvent*>(const_ge);
}
/// @}


/// @brief Get the event weight
///
/// Defaults to the first weight if there are several, 1 if there are none.
///
/// @note Events may increasingly have multiple physically meaningful weights,
/// expressing scale, PDF, etc. systematic variations.
///
/// @todo Generalise/overload for string indices
inline double get_weight(const HepMC::GenEvent* evt, size_t index=0) {
    if (evt->weights().empty()) return 1.0;
    if (index >= evt->weights().size()) throw std::runtime_error("Requested weight array index out of bounds");
    return evt->weights()[index];
}


/// @name Convenience functions for particle/vertex looping
/// @todo Provide functions for particles in/out, different relations, only gen-stable particles, accepting predicates, etc.
/// @{

/// @todo Remove reference-based functions -- always use pointers for HepMC objects?

/// Get a vector of GenParticle pointers on which to iterate as an alternative to HepMC iterators
inline GenParticles particles(const HepMC::GenEvent& ge) {
    GenParticles rtn;
    for (HepMC::GenEvent::particle_const_iterator pi = ge.particles_begin(); pi != ge.particles_end(); ++pi) {
        rtn.push_back(*pi);
    }
    return rtn;
}
/// Get a vector of GenParticle pointers on which to iterate as an alternative to HepMC iterators
inline GenParticles particles(const HepMC::GenEvent* ge) {
    assert(ge);
    return particles(*ge);
}


/// Get a vector of const GenParticle pointers on which to iterate as an alternative to HepMC iterators
inline GenParticlesC const_particles(const HepMC::GenEvent& ge) {
    GenParticlesC rtn;
    for (HepMC::GenEvent::particle_const_iterator pi = ge.particles_begin(); pi != ge.particles_end(); ++pi) {
        rtn.push_back(*pi);
    }
    return rtn;
}
/// Get a vector of const GenParticle pointers on which to iterate as an alternative to HepMC iterators
inline GenParticlesC const_particles(const HepMC::GenEvent* ge) {
    assert(ge);
    return const_particles(*ge);
}



/// Get a vector of GenVertex pointers on which to iterate as an alternative to HepMC iterators
inline GenVertices vertices(const HepMC::GenEvent& ge) {
    GenVertices rtn;
    for (HepMC::GenEvent::vertex_const_iterator vi = ge.vertices_begin(); vi != ge.vertices_end(); ++vi) {
        rtn.push_back(*vi);
    }
    return rtn;
}
/// Get a vector of GenVertex pointers on which to iterate as an alternative to HepMC iterators
inline GenVertices vertices(const HepMC::GenEvent* ge) {
    assert(ge);
    return vertices(*ge);
}


/// Get a vector of const GenVertex pointers on which to iterate as an alternative to HepMC iterators
inline GenVerticesC const_vertices(const HepMC::GenEvent& ge) {
    GenVerticesC rtn;
    for (HepMC::GenEvent::vertex_const_iterator vi = ge.vertices_begin(); vi != ge.vertices_end(); ++vi) {
        rtn.push_back(*vi);
    }
    return rtn;
}
/// Get a vector of const GenVertex pointers on which to iterate as an alternative to HepMC iterators
inline GenVerticesC const_vertices(const HepMC::GenEvent* ge) {
    assert(ge);
    return const_vertices(*ge);
}



/// Get a vector of GenParticle pointers on which to iterate as an alternative to HepMC iterators
///
/// @a range is one of HepMC::relatives, HepMC::descendants, HepMC::children, HepMC::ancestors, HepMC::parents
inline GenParticles particles(HepMC::GenVertex& gv, HepMC::IteratorRange range) {
    GenParticles rtn;
    for (HepMC::GenVertex::particle_iterator pi = gv.particles_begin(range); pi != gv.particles_end(range); ++pi) {
        rtn.push_back(*pi);
    }
    return rtn;
}
/// Get a vector of GenParticle pointers on which to iterate as an alternative to HepMC iterators
///
/// @a range is one of HepMC::relatives, HepMC::descendants, HepMC::children, HepMC::ancestors, HepMC::parents
inline GenParticles particles(HepMC::GenVertex* gv, HepMC::IteratorRange range) {
    assert(gv);
    return particles(*gv, range);
}

/// Get a vector of const GenParticle pointers on which to iterate as an alternative to HepMC iterators
///
/// @a range is one of HepMC::relatives, HepMC::descendants, HepMC::children, HepMC::ancestors, HepMC::parents
inline GenParticlesC const_particles(const HepMC::GenVertex& gv, HepMC::IteratorRange range) {
    GenParticlesC rtn;
    HepMC::GenVertex& gv2 = const_cast<HepMC::GenVertex&>(gv);
    for (HepMC::GenVertex::particle_iterator pi = gv2.particles_begin(range); pi != gv2.particles_end(range); ++pi) {
        rtn.push_back(*pi);
    }
    return rtn;
}
/// Get a vector of const GenParticle pointers on which to iterate as an alternative to HepMC iterators
///
/// @a range is one of HepMC::relatives, HepMC::descendants, HepMC::children, HepMC::ancestors, HepMC::parents
inline GenParticlesC const_particles(const HepMC::GenVertex* gv, HepMC::IteratorRange range) {
    assert(gv);
    return const_particles(*gv, range);
}


/// @name Convenience-named particle relative access functions
/// @{

inline GenParticles children(HepMC::GenVertex& gv) {
    return particles(gv, HepMC::children);
}
inline GenParticles children(HepMC::GenVertex* gv) {
    return particles(gv, HepMC::children);
}
inline GenParticlesC const_children(const HepMC::GenVertex& gv) {
    return const_particles(gv, HepMC::children);
}
inline GenParticlesC const_children(const HepMC::GenVertex* gv) {
    return const_particles(gv, HepMC::children);
}
//
inline GenParticles parents(HepMC::GenVertex& gv) {
    return particles(gv, HepMC::parents);
}
inline GenParticles parents(HepMC::GenVertex* gv) {
    return particles(gv, HepMC::parents);
}
inline GenParticlesC const_parents(const HepMC::GenVertex& gv) {
    return const_particles(gv, HepMC::parents);
}
inline GenParticlesC const_parents(const HepMC::GenVertex* gv) {
    return const_particles(gv, HepMC::parents);
}
//
inline GenParticles descendants(HepMC::GenVertex& gv) {
    return particles(gv, HepMC::descendants);
}
inline GenParticles descendants(HepMC::GenVertex* gv) {
    return particles(gv, HepMC::descendants);
}
inline GenParticlesC const_descendants(const HepMC::GenVertex& gv) {
    return const_particles(gv, HepMC::descendants);
}
inline GenParticlesC const_descendants(const HepMC::GenVertex* gv) {
    return const_particles(gv, HepMC::descendants);
}
//
inline GenParticles ancestors(HepMC::GenVertex& gv) {
    return particles(gv, HepMC::ancestors);
}
inline GenParticles ancestors(HepMC::GenVertex* gv) {
    return particles(gv, HepMC::ancestors);
}
inline GenParticlesC const_ancestors(const HepMC::GenVertex& gv) {
    return const_particles(gv, HepMC::ancestors);
}
inline GenParticlesC const_ancestors(const HepMC::GenVertex* gv) {
    return const_particles(gv, HepMC::ancestors);
}

/// Similar functions for particle relative access direct from GenParticle rather than GenVertex
inline GenParticles children(HepMC::GenParticle& gp) {
    return (gp.end_vertex() != NULL) ? particles(gp.end_vertex(), HepMC::children) : GenParticles();
}
inline GenParticles children(HepMC::GenParticle* gp) {
    return MCUtils::children(*gp);
}
inline GenParticlesC const_children(const HepMC::GenParticle& gp) {
    return (gp.end_vertex() != NULL) ? const_particles(gp.end_vertex(), HepMC::children) : GenParticlesC();
}
inline GenParticlesC const_children(const HepMC::GenParticle* gp) {
    return const_children(*gp);
}
//
inline GenParticles parents(HepMC::GenParticle& gp) {
    return (gp.production_vertex() != NULL) ? particles(gp.production_vertex(), HepMC::parents) : GenParticles();
}
inline GenParticles parents(HepMC::GenParticle* gp) {
    return MCUtils::parents(*gp);
}
inline GenParticlesC const_parents(const HepMC::GenParticle& gp) {
    return (gp.production_vertex() != NULL) ? const_particles(gp.production_vertex(), HepMC::parents) : GenParticlesC();
}
inline GenParticlesC const_parents(const HepMC::GenParticle* gp) {
    return const_parents(*gp);
}
//
inline GenParticles descendants(HepMC::GenParticle& gp) {
    return (gp.end_vertex() != NULL) ? particles(gp.end_vertex(), HepMC::descendants) : GenParticles();
}
inline GenParticles descendants(HepMC::GenParticle* gp) {
    return MCUtils::descendants(*gp);
}
inline GenParticlesC const_descendants(const HepMC::GenParticle& gp) {
    return (gp.end_vertex() != NULL) ? const_particles(gp.end_vertex(), HepMC::descendants) : GenParticlesC();
}
inline GenParticlesC const_descendants(const HepMC::GenParticle* gp) {
    return const_descendants(*gp);
}
//
inline GenParticles ancestors(HepMC::GenParticle& gp) {
    return (gp.production_vertex() != NULL) ? particles(gp.production_vertex(), HepMC::ancestors) : GenParticles();
}
inline GenParticles ancestors(HepMC::GenParticle* gp) {
    return MCUtils::ancestors(*gp);
}
inline GenParticlesC const_ancestors(const HepMC::GenParticle& gp) {
    return (gp.production_vertex() != NULL) ? const_particles(gp.production_vertex(), HepMC::ancestors) : GenParticlesC();
}
inline GenParticlesC const_ancestors(const HepMC::GenParticle* gp) {
    return const_ancestors(*gp);
}

/// @}


/// @name High-level event handling functions
/// @{

/// Find the (most likely) primary vertex position for this event
inline HepMC::ThreeVector primaryVertex(const HepMC::GenEvent* genEvent) {
    // Get the decay vertex of the beam particle
    if (genEvent->valid_beam_particles()) {
        HepMC::GenParticle* p = genEvent->beam_particles().first;
        HepMC::GenVertex* v = p->end_vertex();
        if (v != NULL) return v->point3d();
        else std::cout << "The beam particles have no end vertex" << std::endl;
    }
    // Or get the signal process vertex position
    else if (genEvent->signal_process_vertex() != NULL) {
        return genEvent->signal_process_vertex()->point3d();
    }
    // Or get the production vertex of a final particle whose status is not 1 or 2
    else if (!genEvent->particles_empty()) {
        for (HepMC::GenEvent::particle_const_iterator ip = genEvent->particles_begin(); ip != genEvent->particles_end(); ++ip) {
            if ((*ip)->status() != 1 && (*ip)->status() != 2) {
                const HepMC::GenParticle* p = *ip;
                if (p->production_vertex() != NULL) return p->production_vertex()->point3d();
            }
        }
    }
    // Or get the vertex (either begin or end!) of the particle with barcode == 1
    else {
        HepMC::GenParticle* p = genEvent->barcode_to_particle(1);
        if (p != NULL) {
            if (p->production_vertex() != NULL) return p->production_vertex()->point3d();
            else if (p->end_vertex() != NULL) return p->end_vertex()->point3d();
            else std::cerr << "The first particle has no production vertex and no end vertex" << std::endl;
        }
        else std::cerr << "No particle with barcode 1" << std::endl;
    }
    // No particle with a primary vertex was found, hence returning pv(0,0,0)
    std::cerr << "Primary vertex could not be determined" << std::endl;
    return HepMC::ThreeVector(0., 0., 0.);
}

/// @}
#endif

}
#endif
