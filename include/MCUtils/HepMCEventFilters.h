// -*- C++ -*-
//
// This file is part of MCUtils -- https://gitlab.com/hepcedar/mcutils/
// Copyright (C) 2013-2024 Andy Buckley <andy.buckley@cern.ch>
//
// Embedding of MCUtils code in other projects is permitted provided this
// notice is retained and the MCUtils namespace and include path are changed.
//
#ifndef MCUTILS_HEPMCEVENTFILTERS_H
#define MCUTILS_HEPMCEVENTFILTERS_H

/// @file Functions for filtering HepMC record contents
/// @author Andy Buckley <andy.buckley@cern.ch>


#include <vector>
#include <algorithm>
#include <memory>
#include <type_traits>
#ifdef MCUTILS_HEPMC3
#include "HepMC3/GenEvent.h"
#include "HepMC3/GenParticle.h"
#include "HepMC3/GenVertex.h"
#else
#include "HepMC/GenEvent.h"
#endif


#include "HEPUtils/Utils.h"
#include "MCUtils/HepMCParticleClassifiers.h"
#include "MCUtils/HepMCVertexClassifiers.h"
#include "MCUtils/HepMCParticleFilters.h"



namespace MCUtils {

#ifdef MCUTILS_HEPMC3


  /// @name Friendlier HepMC type typedefs
  /// @{
  typedef std::shared_ptr<HepMC3::GenEvent> GenEventPtr;
  typedef std::shared_ptr<const HepMC3::GenEvent> ConstGenEventPtr;
  typedef std::vector<HepMC3::GenVertexPtr> GenVertices;
  typedef std::vector<HepMC3::ConstGenVertexPtr> ConstGenVertices;
  /// @}

  /// Ensure that T is pointer of following types
  /// @todo Hide this internal detail... anon namespace?
  template<class T>
  using is_GEP = typename std::enable_if<std::is_same<GenEventPtr,T>::value ||
                                         std::is_same<ConstGenEventPtr,T>::value ||
                                         std::is_same<HepMC3::GenEvent*,T>::value ||
                                         std::is_same<const HepMC3::GenEvent*,T>::value, bool>::type;

  /// @name Get particles from a GenEvent which meet general PClassifier criteria
  /// @{

  /// Get all the particles that match a supplied classifier
  template <class T, is_GEP<T> = true>
  inline ConstGenParticles const_particles_match(T ge, const PClassifier& c) {
    std::vector<HepMC3::ConstGenParticlePtr> rtn = ge->particles();
    rtn.erase(remove_if(rtn.begin(), rtn.end(),
                        [c] (HepMC3::ConstGenParticlePtr p) -> bool {return !c(p);}), rtn.end());
    return rtn;
  }


  /// Get all the particles that match a supplied classifier all particles that match
  template <class T, is_GEP<T> = true>
  inline GenParticles particles_match(T ge, const PClassifier& c) {
    return mk_unconst(const_particles_match(mk_const(ge), c));
  }


  /// Get the first particle that matches a supplied classifier
  template <class T, is_GEP<T> = true>
  inline HepMC3::ConstGenParticlePtr const_particle_match(T ge, const PClassifier& c) {
    for (auto p: ge->particles()) {
      if (c(p)) return p;
    }
    return 0;
  }


  /// Get the first particle that matches a supplied classifier
  template <class T, is_GEP<T> = true>
  inline HepMC3::GenParticlePtr particle_match(T ge, const PClassifier& c) {
    return mk_unconst(const_particle_match(mk_const(ge), c));
  }


  /// Get all the particles that match *any* of the supplied classifiers
  /// @todo Remove in favour of a single (lambda) function for simplicity and more general logical combinations.
  template <class T, is_GEP<T> = true>
  inline ConstGenParticles const_particles_match_any(T ge, const std::vector<PClassifier>& classifiers) {
    std::vector<HepMC3::ConstGenParticlePtr> rtn = ge->particles();
    rtn.erase(remove_if(rtn.begin(), rtn.end(),
                        [classifiers] (HepMC3::ConstGenParticlePtr p) -> bool {
                          bool any = false;
                          for (auto c : classifiers) {
                            if (c(p)) {
                              any = true;
                              break;
                            }
                          }
                          return !any;}), rtn.end());
    return rtn;
  }


  // /// Get all the particles that match *any* of the supplied classifiers
  // /// @todo Remove in favour of a single (lambda) function for simplicity and more general logical combinations.
  // template <class T, is_GEP<T> = true>
  // inline GenParticles particles_match_any(T ge, const std::vector<PClassifier>& classifiers) {
  //     return mk_unconst(const_particles_match_any(mk_const(ge), classifiers));
  // }


  //alternative particles that match all criteria
  template <class T, is_GEP<T> = true>
  inline ConstGenParticles const_particles_match_all(T ge, const std::vector<PClassifier>& classifiers) {
    HepMC3::ConstGenParticles rtn = ge->particles();
    rtn.erase(remove_if(rtn.begin(), rtn.end(),
                        [&] (HepMC3::ConstGenParticlePtr p) -> bool {
                          bool allpassed = true;
                          for (auto c : classifiers) {
                            if (!c(p)) {
                              allpassed = false;
                              break;
                            }
                          }
                          return !allpassed;}), rtn.end());
    return rtn;
  }


  // /// Get all the particles that match *all* of the supplied classifiers
  // /// @todo Remove in favour of a single (lambda) function for simplicity and more general logical combinations.
  // template <class T, is_GEP<T> = true>
  // inline GenParticles particles_match_all(T ge, const std::vector<PClassifier>& classifiers) {
  //     return mk_unconst(const_particles_match_all(mk_const(ge), classifiers));
  // }

  /// @}


  /// @name Get particles from a GenEvent which meet specific criteria
  /// @{

  /// Get a vector of GenParticle pointers to particles with given status
  template <class T, is_GEP<T> = true>
  inline ConstGenParticles get_with_status(T evt, int status) {
    return filter_status(const_particles(evt), status);
  }

  /// Get a vector of GenParticle pointers to final state particles
  template <class T, is_GEP<T> = true>
  inline ConstGenParticles get_stable(T evt) {
    return filter_stable(const_particles(evt));
  }

  /// Get a vector of GenParticle pointers to stable particles with given PDG ID code
  template <class T, is_GEP<T> = true>
  inline ConstGenParticles get_id(T evt, long pid) {
    return filter_id(const_particles(evt), pid);
  }

  /// Get a vector of GenParticle pointers to stable particles with given PDG ID code
  template <class T, is_GEP<T> = true>
  inline ConstGenParticles get_stable_id(T evt, long pid) {
    return filter_id(get_stable(evt), pid);
  }

  /// Get a vector of GenParticle pointers to particles with given absolute PDG ID code
  template <class T, is_GEP<T> = true>
  inline ConstGenParticles get_absid(T evt, long pid) {
    return filter_absid(const_particles(evt), pid);
  }

  /// Get a vector of GenParticle pointers to stable particles with given absolute PDG ID code
  template <class T, is_GEP<T> = true>
  inline ConstGenParticles get_stable_absid(T evt, long pid) {
    return filter_absid(get_stable(evt), pid);
  }

  /// Get a vector of GenParticle pointers to charged final state particles
  template <class T, is_GEP<T> = true>
  inline ConstGenParticles get_stable_charged(T evt) {
    return filter_charged(get_stable(evt));
  }

  /// Get a vector of GenParticle pointers to neutral final state particles
  template <class T, is_GEP<T> = true>
  inline ConstGenParticles get_stable_neutral(T evt) {
    return filter_neutral(get_stable(evt));
  }

  /// Get a vector of GenParticle pointers to final state electrons
  template <class T, is_GEP<T> = true>
  inline ConstGenParticles get_stable_electrons(T evt) {
    return filter_absid(get_stable(evt), 11);
  }

  /// Get a vector of GenParticle pointers to final state muons
  template <class T, is_GEP<T> = true>
  inline ConstGenParticles get_stable_muons(T evt) {
    return filter_absid(get_stable(evt), 13);
  }

  /// Get a vector of GenParticle pointers to final state charged leptons (not counting taus)
  template <class T, is_GEP<T> = true>
  inline ConstGenParticles get_stable_chleptons(T evt) {
    std::vector<long> apids;
    apids.push_back(11);
    apids.push_back(13);
    return filter_absids(get_stable(evt), apids);
    // C++11 version: return filter_absids(get_stable(evt), {{11, 13}});
  }

  /// Get a vector of GenParticle pointers to final state neutrinos
  template <class T, is_GEP<T> = true>
  inline ConstGenParticles get_stable_neutrinos(T evt) {
    return filter_neutrinos(get_stable(evt));
  }

  /// Get a vector of GenParticle pointers to decayed taus
  template <class T, is_GEP<T> = true>
  inline ConstGenParticles get_decayed_taus(T evt) {
    /// @todo Make sure there aren't duplicates from a single decay chain
    return filter_absid(get_with_status(evt, 2), 15);
  }

  /// Get a vector of GenParticle pointers to final state photons
  template <class T, is_GEP<T> = true>
  inline ConstGenParticles get_stable_photons(T evt) {
    return filter_id(get_stable(evt), 22);
  }

  /// Get a vector of GenParticle pointers to final state hadrons
  template <class T, is_GEP<T> = true>
  inline ConstGenParticles get_stable_hadrons(T evt) {
    return filter_hadrons(get_stable(evt));
  }

  /// @todo Add get_jet_inputs. Treatments for neutrinos, muons (depends on their history?), and photons?

  /// @todo get_decayed_hadrons, decayed_taus ---- needs the work to clean up decay chains etc.

  /// @todo get_tops --- need work to identify "correct" top

  /// @todo get_Ws, get_Zs --- reco vs. direct... get correct copy for direct way

  /// @}



  /// @name Get vertices from a GenEvent which meet general VClassifier criteria
  /// @{

  /// Get all the vertices that match a supplied classifier
  template <class T, is_GEP<T> = true>
  inline ConstGenVertices const_vertices_match(T ge, const VClassifier& c) {
    std::vector<HepMC3::ConstGenVertexPtr> rtn = ge->vertices();
    rtn.erase(remove_if(rtn.begin(), rtn.end(),
                        [c] (HepMC3::ConstGenVertexPtr vi) -> bool {return !c(vi);}), rtn.end());
    return rtn;
  }


  /// Get all the vertices that match a supplied classifier
  template <class T, is_GEP<T> = true>
  inline GenVertices vertices_match(T ge, const VClassifier& c) {
    return mk_unconst(const_vertices_match(mk_const(ge), c));
  }


  /// Get the first vertex that matches a supplied classifier
  template <class T, is_GEP<T> = true>
  inline HepMC3::ConstGenVertexPtr const_vertex_match(T ge, const VClassifier& c) {
    for (auto vi: ge->vertices()) {
      if (c(vi)) return vi;
    }
    return 0;
  }


  /// Get the first vertex that matches a supplied classifier
  template <class T, is_GEP<T> = true>
  inline HepMC3::GenVertexPtr vertex_match(T ge, const VClassifier& c) {
    return mk_unconst(const_vertex_match(mk_const(ge), c));
  }

  /// @}



  /// @name Event reduction functions
  /// @{

  /// Remove an unwanted particle from the event, collapsing the graph structure consistently
  ///
  /// @todo Rewrite with the batch version as canonical; only sweep once, and maybe add proactive checks for chains of removals.
  template <class T, is_GEP<T> = true>
  inline void ireduce(T ge, std::vector<HepMC3::GenParticlePtr>& gps) {
    if (gps.empty()) return;
    if constexpr(std::is_same<T, HepMC3::GenEvent*>() || std::is_same<T, const HepMC3::GenEvent*>()) {
      if (gps[0]->parent_event() != ge) return;
    } else {
      if (gps[0]->parent_event() != ge.get()) return;
    }

    for (HepMC3::GenParticlePtr gp : gps) {
      // Get start and end vertices
      HepMC3::GenVertexPtr vstart = gp->production_vertex();
      HepMC3::GenVertexPtr vend = gp->end_vertex();

      // Disconnect the unwanted particle from its vertices and delete it
      if (vstart) vstart->remove_particle_out(gp);
      if (vend) vend->remove_particle_in(gp);
      // if (gp) ge->remove_particle(gp); //< @todo More efficient to batch?

      // If start/end vertices are valid and distinct, and this was the only particle that
      // connected them, then reassign the end vertex decay products to the start vertex
      // and rewrite the vertex position as most appropriate.
      /// @note The disconnected end vertex will be picked up by the final "sweeper" loop if necessary.
      /// @note We do the reassigning this way since GV::add_particle_*() modifies the end vertex
      if (vstart && vend && vend != vstart) { //< @todo why is the final condition needed?
        bool is_only_link = true;
        for (HepMC3::ConstGenParticlePtr pchild : vstart->particles_out()) {
          if (pchild->end_vertex() == vend) {
            is_only_link = false;
            break; //< no need to check more possible other vstart-vend links
          }
        }
        // for (HepMC3::ConstGenParticlePtr pchild : const_particles(vstart, "children")) {
        //     if (pchild->end_vertex() == vend) is_only_link = false;
        // }
        if (is_only_link) {
          // std::cout << "START" << std::endl;
          /// @todo Choose whether to delete start or end vertex according to least connections
          if (true) { //delete end
            // Re-set surviving vertex position
            if (vend->position() != HepMC3::FourVector())
              vstart->set_position(vend->position()); //< @todo Always use later position if defined
            // Reattach particles
            // for (HepMC3::GenParticlePtr gp_move : vend->particles_in()) {
            while (vend->particles_in_size() > 0) {
              HepMC3::GenParticlePtr gp_move = vend->particles_in()[0];
              vstart->add_particle_in(gp_move); //< auto-disconnects from vend
              //vend->remove_particle_in(gp_move);
            }
            // for (HepMC3::GenParticlePtr gp_move : vend->particles_out()) {
            while (vend->particles_out_size() > 0) {
              HepMC3::GenParticlePtr gp_move = vend->particles_out()[0];
              vstart->add_particle_out(gp_move); //< auto-disconnects from vend
              //vend->remove_particle_out(gp_move);
            }
            // Delete disconnected vertex
            /// @todo Better to remove batched at the end? YES
            // std::cout << "V for removal: "
            //           << vend->particles_in_size() << " in, "
            //           << vend->particles_out_size() << " out" << std::endl;
            // ge->remove_vertex(vend); ///< @todo Why does this sometimes segfault?
            // std::cout << "END" << std::endl;
          } else { // delete start
            /// @todo
          }

        }
      }
    }

    // Batch-remove particles
    ge->remove_particles(gps);

    // // Sweep up any vertices orphaned by the particle removal
    // /// @todo Can we be a bit more efficient rather than having to run over all vertices every time?
    // ///       Or allow disabling of this clean-up, with a single clean being run at the end of filtering.
    // /// @todo Use neater looping via vertices_match (or iterated vertex_match)
    // /// @todo Also look for and report changes in number of no-parent and no-child vertices
    // auto& gvs = ge->vertices();
    auto isOrphanVtx = [] (HepMC3::GenVertexPtr vp) -> bool { return (vp->particles_in_size() == 0 && vp->particles_out_size() == 0); };
    //for (HepMC3::GenVertexPtr vp : ge->vertices()) { std::cout << isOrphanVtx(vp); } std::cout << std::endl;
    // gvs.erase(remove_if(gvs.begin(), gvs.end(), isOrphanVtx), gvs.end());
    // for (HepMC3::GenVertexPtr vp : gvs) { std::cout << isOrphanVtx(vp); } std::cout << std::endl;
    /// @todo This feels inefficient: need a batch-removal option
    bool clean = false;
    while (!clean) {
      clean = true;
      for (HepMC3::GenVertexPtr gv : ge->vertices()) {
        if (isOrphanVtx(gv)) {
          ge->remove_vertex(gv);
          clean = false;
          break;
        }
      }
      //std::cout << "FOO" << std::endl;
    }
    // for (HepMC3::GenVertexPtr gv : ge->vertices())
    //   if (isOrphanVtx(gv)) ge->remove_vertex(gv);
  }


  template <class T, is_GEP<T> = true>
  inline void ireduce(T ge, HepMC3::GenParticlePtr gp) {
    std::vector<HepMC3::GenParticlePtr> gps{gp};
    ireduce(ge, gps);
  }


  template <class T, is_GEP<T> = true>
  inline void reduce(T ge, HepMC3::GenParticles toremove) {
    while (toremove.size()) {
      HepMC3::GenParticlePtr gp = toremove.back();
      toremove.pop_back();
      reduce(ge, gp);
    }
  }

  // /// Remove unwanted particles from the event, collapsing the graph structure consistently
  // template <class T, is_GEP<T> = true>
  // inline void ireduce(T ge, HepMC3::GenParticles toremove) {
  //     /// @todo Surely there are ways to make this more efficient in batch removal?
  //     while (toremove.size()) {
  //         HepMC3::GenParticlePtr gp = toremove.back();
  //         toremove.pop_back();
  //         ireduce(ge, gp);
  //     }
  // }


  // /// Conversion between HepMC2 and HepMC3 functions based on atlas/athena/Generators/AtlasHepMC/AtlasHepMC/GenEvent.h
  // inline HepMC3::GenParticlePtr  barcode_to_particle(GenEventPtr e, int id ) {
  //     auto particles=e->particles();
  //     for (auto p: particles) {
  //         auto barcode_attr=p->attribute<HepMC3::IntAttribute>("barcode");
  //         if (!barcode_attr) continue;
  //         if (barcode_attr->value()==id) return p;
  //     }
  //     if (id>0&&id<=(int)particles.size()) return particles[id-1];
  //     return  HepMC3::GenParticlePtr();
  // }

  // /// Conversion between HepMC2 and HepMC3 functions based on atlas/athena/Generators/AtlasHepMC/AtlasHepMC/GenParticle.h
  // inline int barcode(HepMC3::GenParticlePtr p) {
  //     if (!p) return 0;
  //     std::shared_ptr<HepMC3::IntAttribute> barcode=p->attribute<HepMC3::IntAttribute>("barcode");
  //     return barcode?(barcode->value()):p->id();
  // }
  // inline int barcode(ConstGenParticlePtr p) {
  //     if (!p) return 0;
  //     std::shared_ptr<HepMC3::IntAttribute> barcode=p->attribute<HepMC3::IntAttribute>("barcode");
  //     return barcode?(barcode->value()):p->id();
  // }


  /// @brief Remove unwanted particles from the event, collapsing the graph structure consistently
  ///
  /// This version operates in-place on a GenEvent, using a vector of particle
  /// barcodes to define which particles are to be removed (it's therefore a bit
  /// more robust but also less efficient than the pointer-based version).
  inline void ireduce(HepMC3::GenEvent* ge, const std::vector<int>& ids_toremove) {
    HepMC3::GenParticles toremove;
    for (int id : ids_toremove) {
      HepMC3::GenParticlePtr gp = ge->particles()[id-1];
      if (gp) toremove.push_back(gp);
    }
    ireduce(ge, toremove);
  }

  /// @todo Generalise to any ptrlike-to-GenEvent with SFINAE
  inline void ireduce(std::shared_ptr<HepMC3::GenEvent> ge, const std::vector<int>& ids_toremove) {
    HepMC3::GenParticles toremove;
    for (int id : ids_toremove) {
      HepMC3::GenParticlePtr gp = ge->particles()[id-1];
      if (gp) toremove.push_back(gp);
    }
    ireduce(ge, toremove);
  }


  /// @brief Remove unwanted particles from the event, collapsing the graph structure consistently
  ///
  /// This version operates on a const GenEvent with return by value of a
  /// reduced heap-allocated *copy*, using a vector of particle ID codes to
  /// define which particles are to be removed (it's therefore a bit more robust
  /// but also less efficient than the pointer-based version).
  ///
  /// The user is responsible for ensuring that the memory associated with the
  /// returned event copy is freed.
  template <class T, is_GEP<T> = true>
  inline T reduce(T ge, const std::vector<int>& ids_toremove) {
    if constexpr(std::is_same<T, HepMC3::GenEvent*>() || std::is_same<T, const HepMC3::GenEvent*>()) {
      T rtnevt = new HepMC3::GenEvent(*ge);
      ireduce(rtnevt, ids_toremove);
      return rtnevt;
    } else {
      T rtnevt = std::make_shared<HepMC3::GenEvent>(*ge);
      ireduce(rtnevt, ids_toremove);
      return rtnevt;
    }
  }


  /// @brief Remove unwanted particles from the event, collapsing the graph structure consistently
  ///
  /// This version operates on a const GenEvent, returning a reduced heap-allocated *copy*. The user is responsible
  /// for ensuring that the memory associated with the returned event is freed.
  template <class T, is_GEP<T> = true>
  inline T reduce(T ge, ConstGenParticles& toremove) {
    std::vector<int> ids_toremove;
    for (HepMC3::ConstGenParticlePtr gp : toremove) {
      if (gp != NULL) ids_toremove.push_back(gp->id());
    }
    return reduce(ge, ids_toremove);
  }


  /// Remove particles from the event which match a classifier function, collapsing the graph structure consistently
  ///
  /// The @c iterative argument determines whether a single pass (iterative =
  /// false) or multi-pass (iterative = true) approach will be taken to the
  /// event reduction. The iterative form is accurate but SLOW!
  template <class T, is_GEP<T> = true>
  inline void ireduce(T ge, const PClassifier& c, bool iterative=false) {
    if (iterative) {
      // Remove one particle, search for a new match, remove, etc. until no matches... SLOW!
      HepMC3::GenParticlePtr gp = particle_match(ge, c);
      while (gp != 0) {
        ireduce(ge, gp); // Reduce in-place
        gp = particle_match(ge, c); // Get next matching particle
      }
    } else {
      // Identify many particles in one run, then remove them all. Might not catch all problem structures
      GenParticles gps = particles_match(ge, c);
      //std::cout << gps.size() << std::endl;
      // if (gps.size()) { for (ConstGenParticlePtr gp : gps) { std::cout << gp->pdg_id() << " "; } std::cout << std::endl; }
      ireduce(ge, gps);
    }
  }


  /// @brief Remove unwanted particles from the event, collapsing the graph structure consistently
  ///
  /// The @c iterative argument determines whether a single pass (iterative =
  /// false) or multi-pass (iterative = true) approach will be taken to the
  /// event reduction. The iterative form is accurate but SLOW!
  ///
  /// This version operates on a const GenEvent, returning a reduced
  /// heap-allocated *copy*. The user is responsible for ensuring that
  /// the memory associated with the returned event is freed.
  template <class T, is_GEP<T> = true>
  inline T reduce(const T ge, const PClassifier& c, bool iterative=false) {
    if constexpr(std::is_same<T, HepMC3::GenEvent*>() || std::is_same<T, const HepMC3::GenEvent*>()) {
      T rtnevt = new HepMC3::GenEvent(*ge);
      ireduce(rtnevt, c, iterative);
      return rtnevt;
    } else {
      T rtnevt = std::make_shared<HepMC3::GenEvent>(*ge);
      ireduce(rtnevt, c, iterative);
      return rtnevt;
    }
  }

  /// @}




#else // HEPMC2





  /// @name Get particles from a GenEvent which meet general PClassifier criteria
  /// @{

  /// Get all the particles that match a supplied classifier
  inline GenParticlesC const_particles_match(const HepMC::GenEvent* ge, const PClassifier& c) {
    std::vector<const HepMC::GenParticle*> rtn;
    for (HepMC::GenEvent::particle_const_iterator pi = ge->particles_begin(); pi != ge->particles_end(); ++pi) {
      if (c(*pi)) rtn.push_back(*pi);
    }
    return rtn;
  }


  /// Get all the particles that match a supplied classifier
  inline GenParticles particles_match(HepMC::GenEvent* ge, const PClassifier& c) {
    return mk_unconst(const_particles_match(mk_const(ge), c));
  }


  /// Get the first particle that matches a supplied classifier
  inline const HepMC::GenParticle* const_particle_match(const HepMC::GenEvent* ge, const PClassifier& c) {
    for (HepMC::GenEvent::particle_const_iterator pi = ge->particles_begin(); pi != ge->particles_end(); ++pi) {
      if (c(*pi)) return *pi;
    }
    return 0;
  }


  /// Get the first particle that matches a supplied classifier
  inline HepMC::GenParticle* particle_match(HepMC::GenEvent* ge, const PClassifier& c) {
    return mk_unconst(const_particle_match(mk_const(ge), c));
  }


  /// Get all the particles that match *any* of the supplied classifiers
  /// @todo Remove in favour of a single (lambda) function for simplicity and more general logical combinations.
  inline GenParticlesC const_particles_match_any(const HepMC::GenEvent* ge, const std::vector<PClassifier>& classifiers) {
    std::vector<const HepMC::GenParticle*> rtn;
    for (HepMC::GenEvent::particle_const_iterator pi = ge->particles_begin(); pi != ge->particles_end(); ++pi) {
      for (const PClassifier& c : classifiers) {
        if (c(*pi)) {
          rtn.push_back(*pi);
          break;
        }
      }
    }
    return rtn;
  }


  /// Get all the particles that match *any* of the supplied classifiers
  /// @todo Remove in favour of a single (lambda) function for simplicity and more general logical combinations.
  inline GenParticles particles_match_any(HepMC::GenEvent* ge, const std::vector<PClassifier>& classifiers) {
    return mk_unconst(const_particles_match_any(mk_const(ge), classifiers));
  }


  /// Get all the particles that match *all* of the supplied classifiers
  /// @todo Remove in favour of a single (lambda) function for simplicity and more general logical combinations.
  inline GenParticlesC const_particles_match_all(const HepMC::GenEvent* ge, const std::vector<PClassifier>& classifiers) {
    std::vector<const HepMC::GenParticle*> rtn;
    for (HepMC::GenEvent::particle_const_iterator pi = ge->particles_begin(); pi != ge->particles_end(); ++pi) {
      bool allpassed = true;
      for (const PClassifier& c : classifiers) {
        if (!c(*pi)) {
          allpassed = false;
          break;
        }
        if (allpassed) rtn.push_back(*pi);
      }
    }
    return rtn;
  }


  /// Get all the particles that match *all* of the supplied classifiers
  /// @todo Remove in favour of a single (lambda) function for simplicity and more general logical combinations.
  inline GenParticles particles_match_all(HepMC::GenEvent* ge, const std::vector<PClassifier>& classifiers) {
    return mk_unconst(const_particles_match_all(mk_const(ge), classifiers));
  }

  /// @}


  /// @name Get particles from a GenEvent which meet specific criteria
  /// @{

  /// Get a vector of GenParticle pointers to particles with given status
  inline GenParticlesC get_with_status(const HepMC::GenEvent* evt, int status) {
    return filter_status(const_particles(evt), status);
  }

  /// Get a vector of GenParticle pointers to final state particles
  inline GenParticlesC get_stable(const HepMC::GenEvent* evt) {
    return filter_stable(const_particles(evt));
  }

  /// Get a vector of GenParticle pointers to stable particles with given PDG ID code
  inline GenParticlesC get_id(const HepMC::GenEvent* evt, long pid) {
    return filter_id(const_particles(evt), pid);
  }

  /// Get a vector of GenParticle pointers to stable particles with given PDG ID code
  inline GenParticlesC get_stable_id(const HepMC::GenEvent* evt, long pid) {
    return filter_id(get_stable(evt), pid);
  }

  /// Get a vector of GenParticle pointers to particles with given absolute PDG ID code
  inline GenParticlesC get_absid(const HepMC::GenEvent* evt, long pid) {
    return filter_absid(const_particles(evt), pid);
  }

  /// Get a vector of GenParticle pointers to stable particles with given absolute PDG ID code
  inline GenParticlesC get_stable_absid(const HepMC::GenEvent* evt, long pid) {
    return filter_absid(get_stable(evt), pid);
  }

  /// Get a vector of GenParticle pointers to charged final state particles
  inline GenParticlesC get_stable_charged(const HepMC::GenEvent* evt) {
    return filter_charged(get_stable(evt));
  }

  /// Get a vector of GenParticle pointers to neutral final state particles
  inline GenParticlesC get_stable_neutral(const HepMC::GenEvent* evt) {
    return filter_neutral(get_stable(evt));
  }

  /// Get a vector of GenParticle pointers to final state electrons
  inline GenParticlesC get_stable_electrons(const HepMC::GenEvent* evt) {
    return filter_absid(get_stable(evt), 11);
  }

  /// Get a vector of GenParticle pointers to final state muons
  inline GenParticlesC get_stable_muons(const HepMC::GenEvent* evt) {
    return filter_absid(get_stable(evt), 13);
  }

  /// Get a vector of GenParticle pointers to final state charged leptons (not counting taus)
  inline GenParticlesC get_stable_chleptons(const HepMC::GenEvent* evt) {
    std::vector<long> apids;
    apids.push_back(11);
    apids.push_back(13);
    return filter_absids(get_stable(evt), apids);
    // C++11 version: return filter_absids(get_stable(evt), {{11, 13}});
  }

  /// Get a vector of GenParticle pointers to final state neutrinos
  inline GenParticlesC get_stable_neutrinos(const HepMC::GenEvent* evt) {
    return filter_neutrinos(get_stable(evt));
  }

  /// Get a vector of GenParticle pointers to decayed taus
  inline GenParticlesC get_decayed_taus(const HepMC::GenEvent* evt) {
    /// @todo Make sure there aren't duplicates from a single decay chain
    return filter_absid(get_with_status(evt, 2), 15);
  }

  /// Get a vector of GenParticle pointers to final state photons
  inline GenParticlesC get_stable_photons(const HepMC::GenEvent* evt) {
    return filter_id(get_stable(evt), 22);
  }

  /// Get a vector of GenParticle pointers to final state hadrons
  inline GenParticlesC get_stable_hadrons(const HepMC::GenEvent* evt) {
    return filter_hadrons(get_stable(evt));
  }

  /// @todo Add get_jet_inputs. Treatments for neutrinos, muons (depends on their history?), and photons?

  /// @todo get_decayed_hadrons, decayed_taus ---- needs the work to clean up decay chains etc.

  /// @todo get_tops --- need work to identify "correct" top

  /// @todo get_Ws, get_Zs --- reco vs. direct... get correct copy for direct way

  /// @}



  /// @name Get vertices from a GenEvent which meet general VClassifier criteria
  /// @{

  /// Get all the vertices that match a supplied classifier
  inline GenVerticesC const_vertices_match(const HepMC::GenEvent* ge, const VClassifier& c) {
    std::vector<const HepMC::GenVertex*> rtn;
    for (HepMC::GenEvent::vertex_const_iterator vi = ge->vertices_begin(); vi != ge->vertices_end(); ++vi) {
      if (c(*vi)) rtn.push_back(*vi);
    }
    return rtn;
  }


  /// Get all the vertices that match a supplied classifier
  inline GenVertices vertices_match(HepMC::GenEvent* ge, const VClassifier& c) {
    return mk_unconst(const_vertices_match(mk_const(ge), c));
  }


  /// Get the first vertex that matches a supplied classifier
  inline const HepMC::GenVertex* const_vertex_match(const HepMC::GenEvent* ge, const VClassifier& c) {
    for (HepMC::GenEvent::vertex_const_iterator vi = ge->vertices_begin(); vi != ge->vertices_end(); ++vi) {
      if (c(*vi)) return *vi;
    }
    return 0;
  }


  /// Get the first vertex that matches a supplied classifier
  inline HepMC::GenVertex* vertex_match(HepMC::GenEvent* ge, const VClassifier& c) {
    return mk_unconst(const_vertex_match(mk_const(ge), c));
  }

  /// @}



  /// @name Event reduction functions
  /// @{

  /// Remove an unwanted particle from the event, collapsing the graph structure consistently
  inline void ireduce(HepMC::GenEvent* ge, HepMC::GenParticle* gp) {
    // Do nothing if for some reason this particle is not actually in this event
    if (gp->parent_event() != ge) return;

    // Get start and end vertices
    HepMC::GenVertex* vstart = gp->production_vertex();
    HepMC::GenVertex* vend = gp->end_vertex();

    // Disconnect the unwanted particle from its vertices and delete it
    if (vstart != NULL) vstart->remove_particle(gp);
    if (vend != NULL) vend->remove_particle(gp);
    delete gp;

    // If start/end vertices are valid and distinct, and this was the only particle that
    // connected them, then reassign the end vertex decay products to the start vertex
    // and rewrite the vertex position as most appropriate.
    /// @note The disconnected end vertex will be picked up by the final "sweeper" loop if necessary.
    /// @note We do the reassigning this way since GV::add_particle_*() modifies the end vertex
    if (vstart != NULL && vend != NULL && vend != vstart) {
      bool is_only_link = true;
      for (const HepMC::GenParticle* pchild : const_particles(vstart, HepMC::children)) {
        if (pchild->end_vertex() == vend) is_only_link = false;
      }
      if (is_only_link) {
        if (vend->position() != HepMC::FourVector())
          vstart->set_position(vend->position()); //< @todo Always use end position if defined... ok?
        while (vend->particles_out_size() > 0) {
          vstart->add_particle_out(*vend->particles_out_const_begin());
        }
        while (vend->particles_in_size() > 0) {
          vstart->add_particle_in(*vend->particles_in_const_begin());
        }
      }
    }

    // Sweep up any vertices orphaned by the particle removal
    /// @todo Can we be a bit more efficient rather than having to run over all vertices every time?
    ///       Or allow disabling of this clean-up, with a single clean being run at the end of filtering.
    /// @todo Use neater looping via vertices_match (or iterated vertex_match)
    /// @todo Also look for and report changes in number of no-parent and no-child vertices
    std::vector<HepMC::GenVertex*> orphaned_vtxs;
    for (HepMC::GenEvent::vertex_const_iterator vi = ge->vertices_begin(); vi != ge->vertices_end(); ++vi) {
      if ((*vi)->particles_in_size() == 0 && (*vi)->particles_out_size() == 0) orphaned_vtxs.push_back(*vi);
    }
    for (HepMC::GenVertex* gv : orphaned_vtxs) delete gv;
  }


  /// Remove unwanted particles from the event, collapsing the graph structure consistently
  inline void ireduce(HepMC::GenEvent* ge, std::vector<HepMC::GenParticle*> toremove) {
    while (toremove.size()) {
      HepMC::GenParticle* gp = toremove.back();
      toremove.pop_back();
      ireduce(ge, gp);
    }
  }


  /// @brief Remove unwanted particles from the event, collapsing the graph structure consistently
  ///
  /// This version operates in-place on a GenEvent, using a vector of particle
  /// barcodes to define which particles are to be removed (it's therefore a bit
  /// more robust but also less efficient than the pointer-based version).
  inline void ireduce(HepMC::GenEvent* ge, const std::vector<int>& barcodes_toremove) {
    std::vector<HepMC::GenParticle*> toremove;
    for (int barcode : barcodes_toremove) {
      HepMC::GenParticle* gp = ge->barcode_to_particle(barcode);
      if (gp != NULL) toremove.push_back(gp);
    }
    ireduce(ge, toremove);
  }


  /// @brief Remove unwanted particles from the event, collapsing the graph structure consistently
  ///
  /// This version operates on a const GenEvent with return by value of a
  /// reduced heap-allocated *copy*, using a vector of particle barcodes to
  /// define which particles are to be removed (it's therefore a bit more robust
  /// but also less efficient than the pointer-based version).
  ///
  /// The user is responsible for ensuring that the memory associated with the
  /// returned event copy is freed.
  inline HepMC::GenEvent* reduce(const HepMC::GenEvent* ge, const std::vector<int>& barcodes_toremove) {
    HepMC::GenEvent* rtnevt = new HepMC::GenEvent(*ge);
    ireduce(rtnevt, barcodes_toremove);
    return rtnevt;
  }


  /// @brief Remove unwanted particles from the event, collapsing the graph structure consistently
  ///
  /// This version operates on a const GenEvent, returning a reduced heap-allocated *copy*. The user is responsible
  /// for ensuring that the memory associated with the returned event is freed.
  inline HepMC::GenEvent* reduce(const HepMC::GenEvent* ge, const std::vector<HepMC::GenParticle*>& toremove) {
    std::vector<int> barcodes_toremove;
    for (const HepMC::GenParticle* gp : toremove) {
      if (gp != NULL) barcodes_toremove.push_back(gp->barcode());
    }
    return reduce(ge, barcodes_toremove);
  }


  /// Remove particles from the event which match a classifier function, collapsing the graph structure consistently
  ///
  /// The @c iterative argument determines whether a single pass (iterative =
  /// false) or multi-pass (iterative = true) approach will be taken to the
  /// event reduction. The iterative form is accurate but SLOW!
  inline void ireduce(HepMC::GenEvent* ge, const PClassifier& c, bool iterative=false) {
    if (iterative) {
      // Remove one particle, search for a new match, remove, etc. until no matches... SLOW!
      HepMC::GenParticle* gp = particle_match(ge, c);
      while (gp != 0) {
        ireduce(ge, gp); // Reduce in-place
        gp = particle_match(ge, c); // Get next matching particle
      }
    } else {
      // Identify many particles in one run, then remove them all. Might not catch all problem structures
      GenParticles gps = particles_match(ge, c);
      ireduce(ge, gps);
    }
  }


  /// @brief Remove unwanted particles from the event, collapsing the graph structure consistently
  ///
  /// The @c iterative argument determines whether a single pass (iterative =
  /// false) or multi-pass (iterative = true) approach will be taken to the
  /// event reduction. The iterative form is accurate but SLOW!
  ///
  /// This version operates on a const GenEvent, returning a reduced heap-allocated *copy*. The user is responsible
  /// for ensuring that the memory associated with the returned event is freed.
  inline HepMC::GenEvent* reduce(const HepMC::GenEvent* ge, const PClassifier& c, bool iterative=false) {
    HepMC::GenEvent* rtnevt = new HepMC::GenEvent(*ge);
    ireduce(rtnevt, c, iterative);
    return rtnevt;
  }

  /// @}

#endif

}

#endif
