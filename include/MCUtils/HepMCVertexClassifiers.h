// -*- C++ -*-
//
// This file is part of MCUtils -- https://gitlab.com/hepcedar/mcutils/
// Copyright (C) 2013-2024 Andy Buckley <andy.buckley@cern.ch>
//
// Embedding of MCUtils code in other projects is permitted provided this
// notice is retained and the MCUtils namespace and include path are changed.
//
#ifndef MCUTILS_HEPMCVERTEXCLASSIFIERS_H
#define MCUTILS_HEPMCVERTEXCLASSIFIERS_H


/// @file Functions for filtering and classifying HepMC GenVertex objects
/// @author Andy Buckley <andy.buckley@cern.ch>

#include <vector>
#include <functional>

#ifdef MCUTILS_HEPMC3
#include "HepMC3/GenEvent.h"
#include "HepMC3/GenVertex.h"
#include "HepMC3/GenParticle.h"
#else
#include "HepMC/GenEvent.h"
#endif


namespace MCUtils {


/// @name GenVertex classifier functions
/// @{

/// Convenient type name for a generic classifier function / function object
#ifdef MCUTILS_HEPMC3
typedef std::function<bool(HepMC3::ConstGenVertexPtr)> VClassifier;
#else
typedef std::function<bool(const HepMC::GenVertex*)> VClassifier;
#endif

/// Determine if the vertex has no incoming particles
template <class T> bool hasNoParents(T v) {
    return v->particles_in_size() == 0;
}

/// Determine if the vertex has no outgoing particles
template <class T>  bool hasNoChildren(T v) {
    return v->particles_out_size() == 0;
}

/// Determine if the vertex has no connected particles
template <class T>  bool isDisconnected(T v) {
    return v->particles_in_size() == 0 && v->particles_out_size() == 0;
}

/// @}


}

#endif
