2025-03-01  Andy Buckley  <andy.buckley@cern.ch>

	* Add an isQuarkonium() function from LHCb.

2024-03-15  Andy Buckley  <andy.buckley@cern.ch>

	* Add a configure script at last: vastly simplifies and improves makefiles.

2024-03-11  Andy Buckley  <andy.buckley@cern.ch>

	* Restructure the pilemc machinery to use the signal event as the
	base for event-building, and to only copy in final-state pile-up
	particles for now. This solves some event corruption issues, and
	keeps the file-sizes minimal for now.

2024-01-10  Andy Buckley  <andy.buckley@cern.ch>

	* Add a little HepMC 2 <-> 3 ASCII-format converter program.

	* Rework HepMC reduction alg for batch removal to be the canonical.

	* Rename in-place reduce function as ireduce.

	* Extensive bugfixing in HepMC3 functions, adding HepMC3 version of hepmcreduce.

2023-02-02  Andy Buckley  <andy.buckley@cern.ch>

	* Backport some charge3 efficiency improvements from Rivet.

2022-12-08  Andy Buckley  <andy.buckley@cern.ch>

	* Release version 1.4.0

2022-12-02  Andy Buckley  <andy.buckley@cern.ch>

	* Add a powers-of-ten lookup table to speed _digit() extraction.

2020-12-04  Andy Buckley  <andy.buckley@cern.ch>

	* Release version 1.3.5

	* Remove requirement of a non-zero core digit in isMagMonopole.

2020-02-26  Andy Buckley  <andy.buckley@cern.ch>

	* Release version 1.3.4

	* Add isHiddenValley with PDG-standard (n,nR) check.

	* Add PDG-standard (n,nR) = (0,0)|(5,9) digit check to isDarkMatter.

	* Add PDG-standard nR = 0 digit check to isExcited.

	* Add isHeavyFlavor canonical alias for isHeavyFlavour.

	* Add isHadron||isQuark checks to hasDown,Up,etc. quark content functions and isHeavyFlavour.

	* Add isBSM() checks to isLepton, isMeson, isHadron, isDiquark, etc. -- these should only return true for SM particles.

	* Introduce preferred nucl{A,Z,Nlambda} preferred aliases, cf. Rivet.

	* Fix Doxygen grouping markers.

2019-08-14  Andy Buckley  <andy.buckley@cern.ch>

	* Release version 1.3.3

	* Fix isSUSY to accept Higgsinos, and add an isSMFundamantal
	function to simplify/clarify. Thanks to Zach Marshall and Sonia
	Carra.

	* Fix isChargedLepton, isNeutrino, and isQuark to allow 4th
	generation, consistent with isLepton.

2017-09-21  Andy Buckley  <andy.buckley@cern.ch>

	* Release version 1.3.2

	* Add isDyon, isQBall, and handling of charges for Q-balls and R-hadrons.

2017-09-20  Andy Buckley  <andy.buckley@cern.ch>

	* Release version 1.3.1

	* Add more BSM PID codes and helper functions, thanks to Zach Marshall.

2016-09-08  Andy Buckley  <andy.buckley@cern.ch>

	* Rename isChLepton to more explicit, less cryptic/ambiguous
	isChargedLepton (with backward-compatibility deprecated alias
	function).

2016-07-20  Andy Buckley  <andy.buckley@cern.ch>

	* Add isStrange/Charm/Bottom(pid) functions.

2016-03-27  Andy Buckley  <andy.buckley@cern.ch>

	* Release version 1.3.0

	* Add PID checking script + data files and 'make check' target.

	* Remove baryon nq ordering rules -- there should be some, but
	some 'valid' PIDs are marked as invalid. The previous
	implementation ignored the valid ordering exemptions for special
	baryon states.

2016-02-22  Andy Buckley  <andy.buckley@cern.ch>

	* Change preferred name of 3*charge function from threeCharge as
	charge3, with deprecation of the old form. Also add abscharge3 and
	abscharge functions, but no absThreeCharge.

2016-02-11  Andy Buckley  <andy.buckley@cern.ch>

	* Improve testpid program and add it to the Makefile.

	* Add generic dark matter codes 51-55, with zero charge, undefined L and J, and appropriate spins.

	* Do not report reggeons as isMeson == true (WTF!!!)

	* Add enforcement of meson, baryon and diquark nqX orderings.

2016-01-29  Andy Buckley  <andy.buckley@cern.ch>

	* 1.2.1 release!

	* Fix dumb switch of _hasQ argument order in hasBottom, etc. (thanks to Margherita Spalla)

2015-11-05  Andy Buckley  <andy.buckley@cern.ch>

	* 1.2.0 release!

	* Require C++11  and remove Boost dependency.

	* Add trivial has<Q>(pid) returns for quark PIDs.

	* Require correct (according to standard) ordering of quark digits in hadrons.

	* Hide some internal utility functions in PIDUtils.h and more code tidying.

	* Fixes to primaryVertex, isFirstWith and isLastWith. Thanks to Scott Snyder.

2015-08-26  Andy Buckley  <andy.buckley@cern.ch>

	* Add required HEPUtils:: namespacing to calls to get_jets()
	function calls with PseudoJet args in Clustering.h

2015-02-26  Andy Buckley  <andy.buckley@cern.ch>

	* 1.1.1 release!

	* Improved functions for promptness and HF/tau decay daughter classification.

2014-10-21  Andy Buckley  <andy.buckley@cern.ch>

	* 1.1.0 release!

2014-10-20  Andy Buckley  <andy.buckley@cern.ch>

	* Adding isFirstWith, isLastWith templated functions: very useful for finding first/last b hadron/quark, etc..

2014-10-10  Andy Buckley  <andy.buckley@cern.ch>

	* Re-enabling isStrange* functions, now with a veto on charm or bottom content.

2014-10-07  Andy Buckley  <andy.buckley@cern.ch>

	* Fix a bug in the isCharmHadron function, also rewriting
	isCharm{Baryon,Meson} and disabling the isStrange* functions.

2014-09-17  Andy Buckley  <andy.buckley@cern.ch>

	* Adding is{Strong,EM,Weak}Interacting and isGluon PID and GenParticle* functions.

	* Moving vectors, FastJet, and generic/math utils to separate HEPUtils package.

2014-07-23  Andy Buckley  <andy.buckley@cern.ch>

	* Fix to logspace: make sure that start and end values are exact,
	not the result of exp(log(x)).

2014-05-28  Andy Buckley  <andy.buckley@cern.ch>

	* Bump version to 1.0.3.

	* Adding Doxygen build and dist to Makefile.

	* Also map some int and double functions from HepMC into PID.

	* Adding copyright and usage notices to all source files.

	* Adding more uses of GenEvent -> PID function mappings via macro.

	* Bump version to 1.0.2.

	* Making use of new functions to enhance the hepmcreduce program.

	* Adding extra PID / GenParticle classifiers for heavy partons and hadrons.

	* Add a new contains(container, val) function in Utils.h.

	* Mapping BSM and QCD identification functions from PID into
	HepMCParticleClassifiers, using a new mapping macro.

	* Big tidy-up of PID utils and adding more classifiers for exotic/BSM stuff.

2014-05-06  Andy Buckley  <andy.buckley@cern.ch>

	* Moving/re-enabling HepMC+FastJet functions, but only in
	Clustering.h, and moving non-HepMC clustering into FastJet.h

	* Adding fastjet:: namespace qualifier on PseudoJet in Clustering.h

2014-04-30  Andy Buckley  <andy.buckley@cern.ch>

	* Bump version to 1.0.1.

	* In the reduce function, use the position of the *end* vertex (if
	it is set) when collapsing links, so that HF hadron displacements
	will be retained.

2014-03-26  Andy Buckley  <andy.buckley@cern.ch>

	* Moving the HepMC vector helpers into a new HepMCVectors.h header.

	* Removing the 'foreach' #define alias for BOOST_FOREACH since it causes problems.

	* Adding in_open_range() and in_closed_range() math functions.

	* Merging math functions with the Gambit version -- a few minor name changes.

	* Rationalising header include chain for Gambit.

2013-11-25  Andy Buckley  <andy.buckley@cern.ch>

	* Marked as version 1.0.0 (first numbered version)

	* Added installation and tarball making make commands.

	* Protecting get_weight() against out of bounds requests (well,
	throwing an exception if so -- at least it explains the problem).

2013-11-22  Andy Buckley  <andy.buckley@cern.ch>

	* Tweaking get_weight to avoid the inline conditional
	return... just in case this is causing a problem apparently
	observed with this function.

2013-11-15  Andy Buckley  <andy.buckley@cern.ch>

	* Adding PID and HepMC isElectron..isTau, isChLepton,
	isDecayedHadron/Tau, isHadronic/LeptonicTau, and hasDescendentWith
	functions.

2013-10-22  Andy Buckley  <andy.buckley@cern.ch>

	* Rewriting findFirst/LastReplica functions to use recursion.

2013-10-19  Andy Buckley  <andy.buckley@cern.ch>

	* Adding findParents() and findChildren() functions based on
	Nataliia's latest versions.

2013-10-12  Andy Buckley  <andy.buckley@cern.ch>

	* Fix to unary P4::operator- thanks to David Grellscheid.

2013-10-11  Andy Buckley  <andy.buckley@cern.ch>

	* Important mathematical fix in setting P4 from rapidity.

	* Some vector improvements and a new MCUtils.h convenience header.

	* Tidying/restructuring to factorize the FastJet dependence.

2013-10-10  Andy Buckley  <andy.buckley@cern.ch>

	* Sanity restructuring in hepmcreduce, including dropping use of
	the lambda function (now that we were naming it!) and all-vector
	classifier approaches, defining the classifier outside the event
	loop (!) and adding testing and printout of worrying event
	metrics.

	* Change the core reduce function to only operate on one particle
	at a time, and add iterative/traditional versions of reduce which
	operate direct from a PClassifier rather than a list of to_remove.

	* Use the HepMC and fastjet namespaces implicitly inside MCUtils.

	* Move typedefs, mk_const, etc. into specific *Utils.h headers and
	add HepMCVertexUtils.h

	* Adding (const_)particle_match and (const_)vertex_match functions
	for iterative event reduction.

	* Adding mk_const functions for single GenEvent, GenParticle, and
	GenVertex pointers.

	* Renaming Classifier to PClassifier, and adding VClassifier.

	* Adding HepMCVertexClassifiers.h

2013-10-08  Andy Buckley  <andy.buckley@cern.ch>

	* Improving/fixing the reduce algorithm to deal with merging
	vertices which have other incoming/outgoing particles.

	* Makefile and hepmcreduce UI improvements.

2013-09-05  Andy Buckley  <andy.buckley@cern.ch>

	* Adding a demonstrator of using a C++11 lambda function to
	combine classifiers for event reduction.

	* Adding P4 coordinate set methods, and converting the static mk
	functions to use them.

	* Adding extra P4 creation functions, using pT rather than E.

2013-09-04  Andy Buckley  <andy.buckley@cern.ch>

	* Serious bugfix in P4 value setting using rapidity.

2013-04-25  Andy Buckley  <andy.buckley@cern.ch>

	* Improvements to the P4 API, from changes made in Gambit.

	* Adding and using loop-particle and low-energy photon removing functors.

	* Rewrite of the reduce function: much better now

2013-04-09  Andy Buckley  <andy.buckley@cern.ch>

	* Adding some functions from the ATLAS generators code, to allow
	easier syncing of this package with ATLAS.

2013-04-08  Andy Buckley  <andy.buckley@cern.ch>

	* Adding P4 Doxygen documentation strings.

	* Starting changelog: collection of PID, filter, classifier,
	etc. functions and the P4 vector class already in place.
