#include <random>
#include "MCUtils/HepMCVectors.h"
#include "HepMC3/FourVector.h"
#include "almostEqual.h"


int main() {

    double lower_bound=-100000, upper_bound=100000;
    std::uniform_real_distribution<double> unif(lower_bound,upper_bound);
    std::default_random_engine generator(time(0));

    HEPUtils::P4 v;

    double px, py, pz, m, e;
    for (int i=0; i<1000; i++){
        px = unif(generator);
        py = unif(generator);
        pz = unif(generator);
        m = unif(generator);

        e = sqrt(px*px + py*py + pz*pz + m*m);
        HepMC3::FourVector Fv( px, py, pz, e );
        v = MCUtils::mk_p4(Fv);

        if (!almost_equal(v.E(), e, 1)) {
            return 1;
        }

    }
    return 0;
}
